﻿namespace Silkroad.Framework.Constants
{
    public static class TemplateMessage
    {
        public static class Error
        {

        }

        public static class Warning
        {
            public const string SQL_EXCEPTION = "An sql error occurred while {0}. More information: {1}";
            public const string EXPLOIT = "Client({0}:{1}) detected client spawn exploit.";
            public const string ZERK_EXPLOIT = "Client({0}:{1}) detected zerk state exploit.";
            public const string IWA_EXPLOIT = "Client({0}:{1}) detected iwa exploit.";
            public const string AVATAR_MAGIC_EXPLOIT = "Client({0}:{1}) detected avatar magic option exploit.";
            public const string PET_EXPLOIT = "You have more than 2 grab pets, this can crash your SR_GameServer.exe";
        }

        public static class Infomation
        {
            public const string IP_LIMIT = "(User {0} - IP {1}) have reached the maximum amount of Users per IP.";
            public const string PC_LIMIT = "(User {0} - HardwareId {1}) have reached the maximum amount of Users per PC.";
            public const string JOB_PC_LIMIT = "(User {0} - HardwareId {1}) have reached the maximum amount of JOB per PC.";
            public const string FTW_PC_LIMIT = "(User {0} - HardwareId {1}) have reached the maximum amount of FTW per PC.";
            public const string CTF_PC_LIMIT = "(User {0} - HardwareId {1}) have reached the maximum amount of CTF per PC.";
            public const string BA_PC_LIMIT = "(User {0} - HardwareId {1}) have reached the maximum amount of BA per PC.";
        }
    }
}

