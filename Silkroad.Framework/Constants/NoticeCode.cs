﻿namespace Silkroad.Framework.Constants
{
    public static class NoticeCode
    {
        public const string AVATAR_EXPLOIT = "UIIT_STT_AVATAR_EXPLOIT";
        public const string EXIT_DELAY = "UIIT_STT_EXIT_DELAY";
        public const string EXCHANGE_DELAY = "UIIT_STT_EXCHANGE_DELAY";
        public const string RESTART_DELAY = "UIIT_STT_RESTART_DELAY";
        public const string GLOBAL_DELAY = "UIIT_STT_GLOBAL_DELAY";
        public const string STALL_DELAY = "UIIT_STT_STALL_DELAY";
        public const string STALL_EXPLOIT = "UIIT_STT_STALL_EXPLOIT";
        public const string RES_SCROLL_DELAY = "UIIT_STT_RES_SCROLL_DELAY";
        public const string REVERSE_SCROLL_DELAY = "UIIT_STT_REVERSE_SCROLL_DELAY";
        public const string RESTART_DISABLE = "UIIT_STT_RESTART_DISABLE";
        public const string ZERK_DELAY = "UIIT_STT_ZERK_DELAY";
        public const string DISABLE_ZERK_FELLOW = "UIIT_STT_DISABLE_ZERK_FELLOW";
        public const string DISABLE_JG_FW_REG = "UIIT_STT_DISABLE_JG_FW_REG";
        public const string DISABLE_BA_FW_REG = "UIIT_STT_DISABLE_BA_FW_REG";
        public const string DISABLE_HT_FW_REG = "UIIT_STT_DISABLE_HT_FW_REG";
        public const string DISABLE_TAX_RATE_CHANGE = "UIIT_STT_DISABLE_TAXES";
        public const string NOT_ALLOWED_SPECIAL_CHARACTERS = "UIIT_STT_NOT_ALLOWED_SPECIAL_CHARACTERS";
        public const string CHAT_FILTER = "UIIT_STT_CHAT_FILTER";
        public const string GLOBAL_LEVEL = "UIIT_STT_GLOBAL_REQLVL";
        public const string JOB_RES_SCROLL = "UIIT_STT_JOB_RES_SCROLL";
        public const string DISABLE_FELLOW_JOB = "UIIT_STT_DISABLE_FELLOW_JOB";
        public const string JOB_TRACE = "UIIT_STT_JOB_TRACE";
        public const string FTW_TRACE = "UIIT_STT_FW_TRACE";
        public const string FTW_RES_SCROLL = "UIIT_STT_FTW_RES_SCROLL";
        public const string GUILD_LIMIT = "UIIT_STT_GUILDMEMBER_LIMIT";
        public const string UNION_LIMIT = "UIIT_STT_UNIONMEMBER_LIMIT";
        public const string ACADEMY_CREATION = "UIIT_STT_ACADEMY_CREATION";
        public const string ACADEMY_INVITE = "UIIT_STT_ACADEMY_INVITE";
        public const string IP_LIMIT = "UIIT_STT_IP_LIMIT";
        public const string PC_LIMIT = "UIIT_STT_JOB_PC_LIMIT";
        public const string JOB_PC_LIMIT = "UIIT_STT_JOB_PC_LIMIT";
        public const string BA_PC_LIMIT = "UIIT_STT_BA_PC_LIMIT";
        public const string FTW_PC_LIMIT = "UIIT_STT_FW_PC_LIMIT";
        public const string CTF_PC_LIMIT = "UIIT_STT_CTF_PC_LIMIT";
        public const string WELCOME_MSG = "UIIT_STT_WELCOME_MSG";
        public const string JOB_LEVEL = "UIIT_STT_JOB_LEVEL";
        public const string CTF_LEVEL = "UIIT_STT_CTF_LEVEL";
        public const string BA_LEVEL = "UIIT_STT_BA_LEVEL";
        public const string BLOCK_SKILL = "UIIT_STT_SKILL_PREVENTION";
        public const string BLOCK_JOB_SKILL = "UIIT_STT_JOBSKILL_PREVENTION";
        public const string BLOCK_FTW_SKILL = "UIIT_STT_FWSKILL_PREVENTION";
        public const string JOB_REVERSE_LAST_RECALL_POINT = "UIIT_STT_JOB_RESSLRP";
        public const string JOB_REVERSE_DEATH_POINT = "UIIT_STT_JOB_RESSBTDP";
        public const string JOB_REVERSE_MAP_POINT = "UIIT_STT_JOB_MAPLOC";
        public const string DISABLE_GOLDITEM_DROP_INTOWN = "UIIT_STT_DISABLE_GOLDITEM_DROP_INTOWN";
        public const string AUTO_REPAIR_ITEM = "UIIT_STT_AUTO_REPAIR_ITEM";
    }
}
