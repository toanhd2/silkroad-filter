﻿using Silkroad.Framework.Enums;

namespace Silkroad.Framework.Models
{
    public class CharacterInfo
    {
        public JobType JobType { get; set; }
        public bool JobState { get; set; }
        public short LatestRegion { get; set; }
    }
}
