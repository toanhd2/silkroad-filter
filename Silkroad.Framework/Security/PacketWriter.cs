﻿using System.IO;

namespace Silkroad.Framework.Security
{
    public sealed class PacketWriter : BinaryWriter
    {
        private readonly MemoryStream _stream;

        public PacketWriter()
        {
            _stream = new MemoryStream();
            this.OutStream = _stream;
        }

        public byte[] GetBytes()
        {
            return _stream.ToArray();
        }
    }
}
