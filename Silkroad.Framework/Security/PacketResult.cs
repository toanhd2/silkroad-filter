﻿using Silkroad.Framework.Enums;
using System.Collections;
using System.Collections.Generic;

namespace Silkroad.Framework.Security
{
    public struct PacketResult : IEnumerable<Packet>
    {
        #region Fields (Static)

        public static readonly PacketResult None = new PacketResult(PacketResultAction.None);
        public static readonly PacketResult Ignore = new PacketResult(PacketResultAction.Ignore);
        public static readonly PacketResult Disconnect = new PacketResult(PacketResultAction.Disconnect);

        #endregion Fields (Static)

        #region Fields

        private readonly PacketResultAction _action;
        private readonly List<Packet> _packets;

        #endregion Fields

        #region Properties

        public PacketResultAction Action => _action;

        #endregion Properties

        #region Constructor

        public PacketResult(PacketResultAction action)
        {
            _action = action;
            _packets = new List<Packet>();
        }

        public PacketResult(PacketResultAction action, Packet packet)
        {
            _action = action;
            _packets = new List<Packet> { packet };
        }

        public PacketResult(PacketResultAction action, IEnumerable<Packet> packets)
        {
            _action = action;
            _packets = new List<Packet>(packets);
        }

        #endregion Constructor

        #region IEnumerable

        public Packet this[int index] => _packets[index];
        public IEnumerator<Packet> GetEnumerator() => _packets.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

        #endregion IEnumerable

        #region Methods

        public void Add(Packet packet)
        {
            _packets.Add(packet);
        }

        public void Add(IEnumerable<Packet> packets)
        {
            _packets.AddRange(packets);
        }

        #endregion Methods
    }
}
