﻿using Silkroad.Framework.Enums;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Network;
using System.Collections.Generic;

namespace Silkroad.Framework.Security
{
    public class PacketManager<T> where T : Session
    {
        private readonly Dictionary<ushort, IPacketHandler<T>> _moduleHandler;
        private readonly Dictionary<ushort, IPacketHandler<T>> _remoteHandler;

        public PacketManager()
        {
            _moduleHandler = new Dictionary<ushort, IPacketHandler<T>>();
            _remoteHandler = new Dictionary<ushort, IPacketHandler<T>>();
        }

        public void RegisterHandler(PacketType source, Opcode opcode, IPacketHandler<T> packetHandler) {
            if (source == PacketType.Client)
            {
                _moduleHandler[(ushort)opcode] = packetHandler;
            } else
            {
                _remoteHandler[(ushort)opcode] = packetHandler;
            }
        }

        public PacketResult Handle(PacketType source, T session, Packet packet)
        {
            switch (source)
            {
                case PacketType.Server:
                    if (_remoteHandler.ContainsKey(packet.Opcode))
                        return _remoteHandler[packet.Opcode].Handle(session, packet);
                    break;

                case PacketType.Client:
                    if (_moduleHandler.ContainsKey(packet.Opcode))
                        return _moduleHandler[packet.Opcode].Handle(session, packet);
                    break;
            }

            return PacketResult.None;
        }
    }
}
