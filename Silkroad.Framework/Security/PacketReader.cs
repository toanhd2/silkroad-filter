﻿using System.IO;

namespace Silkroad.Framework.Security
{
    public sealed class PacketReader : BinaryReader
    {
        public PacketReader(byte[] input)
            : base(new MemoryStream(input, false))
        {
        }

        public PacketReader(byte[] input, int index, int count)
            : base(new MemoryStream(input, index, count, false))
        {
        }
    }
}
