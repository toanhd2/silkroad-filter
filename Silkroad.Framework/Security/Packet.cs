﻿using Serilog;
using Silkroad.Framework.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Silkroad.Framework.Security
{
    /// <summary>
    /// Network packet class
    /// </summary>
    public class Packet
    {
        public ushort Opcode { get; }
        public bool Encrypted { get; }
        public bool Massive { get; }
        public bool Locked { get; private set; }
        public long Length => _stream.Length;
        private readonly object _lock = new object();

        private readonly MemoryStream _stream;
        private readonly BinaryReader _reader;
        private readonly BinaryWriter _writer;

        public Packet(Opcode opcode) : this((ushort) opcode)
        {
        }

        public Packet(Opcode opcode, bool encrypted) : this((ushort)opcode, encrypted)
        {
        }

        public Packet(ushort opcode)
        {
            _stream = new MemoryStream();
            _reader = new BinaryReader(_stream);
            _writer = new BinaryWriter(_stream);
            Opcode = opcode;
            Encrypted = false;
            Massive = false;
            Locked = false;
            _lock = new object();
        }

        public Packet(ushort opcode, bool encrypted) : this(opcode)
        {
            Encrypted = encrypted;
        }

        public Packet(ushort opcode, bool encrypted, bool massive)
        {
            if (encrypted && massive)
            {
                throw new Exception("[Packet::Packet] Packets cannot both be massive and encrypted!");
            }

            _stream = new MemoryStream();
            _reader = new BinaryReader(_stream);
            _writer = new BinaryWriter(_stream);
            Opcode = opcode;
            Encrypted = encrypted;
            Massive = massive;
            Locked = false;
        }

        public Packet(ushort opcode, bool encrypted, bool massive, byte[] bytes, int offset, int length)
        {
            if (encrypted && massive)
            {
                throw new Exception("[Packet::Packet] Packets cannot both be massive and encrypted!");
            }

            _stream = new MemoryStream();
            _reader = new BinaryReader(_stream);
            _writer = new BinaryWriter(_stream);
            _writer.Write(bytes, offset, length);
            Opcode = opcode;
            Encrypted = encrypted;
            Massive = massive;
            Locked = false;
        }

        public void Lock()
        {
            _reader.BaseStream.Seek(0, SeekOrigin.Begin);
            Locked = true;
        }

        public byte[] GetDataBytes()
        {
            if (!Locked)
                Lock();

            long origPos = _reader.BaseStream.Position;
            _reader.BaseStream.Seek(0, SeekOrigin.Begin);

            byte[] bytes = _reader.ReadBytes((int)_stream.Length);

            _reader.BaseStream.Seek(origPos, SeekOrigin.Begin);

            return bytes;

        }

        public byte[] GetBytes() => GetDataBytes();
        public int RemainRead => (int)(_stream.Length - _reader.BaseStream.Position);
        public void SeekToBeginning() => _stream.Seek(0, SeekOrigin.Begin);

        public T ReadValue<T>()
        {
            if (!Locked)
                throw new InvalidOperationException("Cannot Read from an unlocked Packet.");
            
            lock (_lock)
            {
                T result = default;
                Type type = typeof(T);

                try
                {
                    switch (type)
                    {
                        case var _ when type == typeof(bool):
                            result = (T)Convert.ChangeType(_reader.ReadBoolean(), type);
                            break;

                        case var _ when type == typeof(byte):
                            result = (T)Convert.ChangeType(_reader.ReadByte(), type);
                            break;

                        case var _ when type == typeof(sbyte):
                            result = (T)Convert.ChangeType(_reader.ReadSByte(), type);
                            break;

                        case var _ when type == typeof(short):
                            result = (T)Convert.ChangeType(_reader.ReadInt16(), type);
                            break;

                        case var _ when type == typeof(ushort):
                            result = (T)Convert.ChangeType(_reader.ReadUInt16(), type);
                            break;

                        case var _ when type == typeof(int):
                            result = (T)Convert.ChangeType(_reader.ReadInt32(), type);
                            break;

                        case var _ when type == typeof(uint):
                            result = (T)Convert.ChangeType(_reader.ReadUInt32(), type);
                            break;

                        case var _ when type == typeof(long):
                            result = (T)Convert.ChangeType(_reader.ReadUInt64(), type);
                            break;

                        case var _ when type == typeof(ulong):
                            result = (T)Convert.ChangeType(_reader.ReadUInt64(), type);
                            break;

                        case var _ when type == typeof(float):
                            result = (T)Convert.ChangeType(_reader.ReadSingle(), type);
                            break;

                        case var _ when type == typeof(string):
                            ushort length = _reader.ReadUInt16();
                            byte[] strBytes = _reader.ReadBytes(length);
                            result = (T)Convert.ChangeType(Encoding.ASCII.GetString(strBytes), type);
                            break;

                        default:
                            throw new InvalidOperationException($"Cannot read {type.Name} value from packet with Opcode {Opcode: X4}.");
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, ex.Message);
                }

                return result;
            }
        }

        public List<T> ReadList<T>(int count)
        {
            if (!Locked)
                throw new InvalidOperationException("Cannot Read from an unlocked Packet.");

            List<T> result = new List<T>(count);
            for (int i = 0; i < count; i++)
                result.Add(ReadValue<T>());
            return result;
        }

        public byte[] ReadByteArray(int count)
        {
            if (!Locked)
                throw new InvalidOperationException("Cannot Read from an unlocked Packet.");

            return _reader.ReadBytes(count);
        }

        public void WriteValue<T>(dynamic value)
        {
            if (Locked)
                throw new InvalidOperationException("Cannot Read from an unlocked Packet.");

            value = (T)value;

            if (value is string str)
            {
                byte[] bytes = Encoding.ASCII.GetBytes(str);
                _writer.Write((ushort)bytes.Length);
                _writer.Write(bytes);
                return;
            }

            _writer.Write(value);
        }

        public void WriteList<T>(params dynamic[] values)
        {
            if (Locked)
                throw new InvalidOperationException("Cannot Read from an unlocked Packet.");

            foreach (var value in values)
                WriteValue<T>(value);
        }

        public void WriteByteArray(byte[] data)
        {
            if (Locked)
                throw new InvalidOperationException("Cannot Read from an unlocked Packet.");

            _writer.Write(data);
        }

        public void WriteByteArray(byte[] data, int offset, int count)
        {
            if (Locked)
                throw new InvalidOperationException("Cannot Read from an unlocked Packet.");

            _writer.Write(data, offset, count);
        }
    }
}
