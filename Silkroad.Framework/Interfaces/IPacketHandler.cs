﻿using Silkroad.Framework.Security;

namespace Silkroad.Framework.Interfaces
{
    /// <summary>
    /// The packet handler interface.
    /// </summary>
    public interface IPacketHandler<in T> where T : ISession
    {
        /// <summary>
        /// Handle Packet
        /// </summary>
        /// <param name="session"></param>
        /// <param name="packet"></param>
        /// <returns></returns>
        PacketResult Handle(T session, Packet packet);
    }
}
