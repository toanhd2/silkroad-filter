﻿namespace Silkroad.Framework.Interfaces
{
    public delegate void ReceivedEventHandler(byte[] buffer, int offset, int count);
    public delegate bool DisconnectedEventHandler();
    public interface IClient
    {
        /// <summary>
        /// Fired once some data is received.
        /// </summary>
        event ReceivedEventHandler OnReceived;

        /// <summary>
        /// Fired once client disconnected.
        /// </summary>
        event DisconnectedEventHandler OnDisconnected;

        /// <summary>
        /// Gets the value indicating if the connection is established.
        /// </summary>
        bool IsConnected { get; }

        /// <summary>
        /// Disconnect the client (asynchronous)
        /// </summary>
        /// <returns>'true' if the client was successfully disconnected, 'false' if the client is already disconnected</returns>
        bool DisconnectAsync();

        /// <summary>
        /// Connect the client (asynchronous)
        /// </summary>
        /// <returns>'true' if the client was successfully connected, 'false' if the client failed to connect</returns>
        bool ConnectAsync();

        /// <summary>
        /// Handle buffer received notification
        /// </summary>
        /// <param name="buffer">Received buffer</param>
        /// <param name="offset">Received buffer offset</param>
        /// <param name="size">Received buffer size</param>
        /// <remarks>
        /// Notification is called when another chunk of buffer was received from the server
        /// </remarks>
        void OnReceivedFromServer(byte[] buffer, int offset, int size);

        /// <summary>
        /// Send data to the server (asynchronous)
        /// </summary>
        /// <param name="buffer">Buffer to send</param>
        /// <param name="offset">Buffer offset</param>
        /// <param name="size">Buffer size</param>
        /// <returns>'true' if the data was successfully sent, 'false' if the client is not connected</returns>
        bool SendAsync(byte[] buffer, int offset, int size);
    }
}
