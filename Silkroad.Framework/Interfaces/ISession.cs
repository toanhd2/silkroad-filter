﻿using Silkroad.Framework.Security;

namespace Silkroad.Framework.Interfaces
{
    public interface ISession
    {
        /// <summary>
        /// Token of current session
        /// </summary>
        public uint Token { get; set; }
        /// <summary>
        /// Ip address of current session
        /// </summary>
        string IpAddress { get; set; }
        /// <summary>
        /// Mac address of current session
        /// </summary>
        string HardwareId { get; set; }
        SrSecurity ClientSecurity { get; set; }
        SrSecurity RemoteSecurity { get; set; }
        /// <summary>
        /// Send data to the client (asynchronous)
        /// </summary>
        /// <param name="buffer">Buffer to send</param>
        /// <param name="offset">Buffer offset</param>
        /// <param name="size">Buffer size</param>
        /// <returns>'true' if the data was successfully sent, 'false' if the session is not connected</returns>
        bool SendAsync(byte[] buffer, int offset, int size);
        /// <summary>
        /// Handle buffer received notification
        /// </summary>
        /// <param name="buffer">Received buffer</param>
        /// <param name="offset">Received buffer offset</param>
        /// <param name="size">Received buffer size</param>
        /// <remarks>
        /// Notification is called when another chunk of buffer was received from the server
        /// </remarks>
        void OnReceivedFromServer(byte[] buffer, int offset, int size);

        /// <summary>
        /// Handle buffer received notification
        /// </summary>
        /// <param name="buffer">Received buffer</param>
        /// <param name="offset">Received buffer offset</param>
        /// <param name="size">Received buffer size</param>
        /// <remarks>
        /// Notification is called when another chunk of buffer was received from the client
        /// </remarks>
        void OnReceivedFromClient(byte[] buffer, int offset, int size);

        /// <summary>
        /// Transfer data to client
        /// </summary>
        void TransferToClientAsync();

        /// <summary>
        /// Transfer data to server
        /// </summary>
        void TransferToServerAsync();

        /// <summary>
        /// Disconnect the client (asynchronous)
        /// </summary>
        /// <returns>'true' if the client was successfully disconnected, 'false' if the client is already disconnected</returns>
        bool Disconnect();
    }
}
