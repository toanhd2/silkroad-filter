﻿using System.Collections;
using System.Collections.Generic;

namespace Silkroad.Framework.Configure
{
    public static class Settings
    {
        #region GENERAL
        public static string ACC_DB = "SRO_VT_ACCOUNT";
        public static string SHA_DB = "SRO_VT_SHARD";
        public static string LOG_DB = "LOG_DB";
        public static string FILTER_DB = "FILTER";
        #endregion

        #region GATEWAY_AGENT
        // PROXY IP SETTINGS
        public static string PROXY_IP = string.Empty;

        // BLOCK STATUS
        public static bool BLOCK_STATUS = false;

        // REMOVE CAPTCHA?
        public static bool DISABLE_CAPTCHA = true;

        // CAPTCHA CHAR / IF REMOVE?
        public static string CAPTCHA_CHAR = "1";

        #endregion

        #region SERVER_INFO
        public static string SERVER_NAME = "YenNhi";
        public static ushort FAKE_PLAYERS = 499;
        public static ushort MAX_PLAYERS = 500;
        public static ushort SHARD_ID = 64;
        #endregion

        #region USER
        public static bool GM_LOGIN = false;
        public static bool GM_ANTI_INVISIBLE = true;
        public static List<string> GM_ACCOUNTS = new List<string>() { "1" };
        public static List<string> PRIV_IP = new List<string>();
        public static List<string> USER_WHITELIST = new List<string>();
        #endregion

        #region LIMIT
        public static int IP_LIMIT = 0;
        public static int GLOBAL_IP_LIMIT = 0;
        public static int CAFE_IP_LIMIT = 0;
        public static int PC_LIMIT = 0;
        public static int PLUS_LIMIT = 0;
        public static int JOB_PC_LIMIT = 0;
        public static int CTF_PC_LIMIT = 0;
        public static int BA_PC_LIMIT = 0;
        public static int FTW_PC_LIMIT = 0;
        public static int GUILD_LIMIT = 1;
        public static int UNION_LIMIT = 1;
        #endregion

        #region LEVEL
        public static int CTF_LEVEL = 0;
        public static int BA_LEVEL = 0;
        public static int GLOBAL_LEVEL = 0;
        public static int STALL_LEVEL = 0;
        public static int EXCHANGE_LEVEL = 0;
        public static int JOB_LEVEL = 0;
        #endregion

        #region DELAY
        public static int STALL_DELAY = 10;
        public static int EXCHANGE_DELAY = 10;
        public static int GLOBAL_DELAY = 10;
        public static int REVERSE_DELAY = 10;
        public static int LOGOUT_DELAY = 10;
        public static int RESTART_DELAY = 10;
        public static int ZERK_DELAY = 0;
        public static int RESCURRENT_DELAY = 20;
        #endregion

        #region MISC
        public static bool WELCOME_MSG = true;
        public static bool RESTART_DISABLE = false;
        public static bool DISABLE_AVATAR_BLUES = false;
        public static bool ENABLE_PLUS_NOTICE = false;
        public static int PLUS_NOTICE_REQ = 2;
        public static bool DISABLED_ACADEMY = true;
        public static bool DISABLE_TAX_RATE_CHANGE = false;
        public static bool TOWN_DROP_ITEM = false;
        public static bool ANTI_ZERK_PVP = true;
        public static bool DISABLE_FELLOW_UNDER_ZERK = true;
        public static bool DISABLE_FELLOW_UNDER_JOB = true;
        public static bool DISABLE_JG_FW_REG = true;
        public static bool DISABLE_BA_FW_REG = false;
        public static bool DISABLE_HT_FW_REG = false;
        public static bool LOG_UNIQUE = true;
        public static bool LOG_GLOBALCHAT = true;
        public static bool AFK_DETECTION = false;
        public static bool AUTO_REPAIR_ITEM = true;
        #endregion

        #region SOME_LIST_SHIT
        public static Hashtable Bad_Opcodes = new Hashtable();
        public static List<ushort> Server_Opcodes_Gateway = new List<ushort>();
        public static List<ushort> Server_Whitelisted_Gateway = new List<ushort>();
        public static List<ushort> Server_Opcodes_Agent = new List<ushort>();
        public static List<ushort> Server_Whitelisted_Agent = new List<ushort>();
        public static List<string> Ip_List = new List<string>();
        public static List<string> Flood_List = new List<string>();
        public static List<string> NETCAFE_IPS = new List<string>();
        public static List<string> Limit_Bypass = new List<string>();
        public static List<string> Ban_List = new List<string>();
        public static List<int> FortressRegions = new List<int>();
        public static List<int> SpecialRegions = new List<int>();
        public static List<short> Regions = new List<short>();
        public static List<uint> FTW_TELEPORTS = new List<uint>();
        public static List<uint> Jobcavetele = new List<uint>();
        public static List<short> TOWN_REGIONS = new List<short>();
        public static List<int> BLOCKED_FTW_SKILLS = new List<int>();
        public static List<int> BLOCKED_JOB_SKILLS = new List<int>();
        public static List<int> BLOCKED_SKILLS = new List<int>();
        public static List<string> BadWords = new List<string> { "athena", "toanmk" };
        public static List<string> GatewayCheckPort = new List<string>();
        public static List<string> AgentCheckPort = new List<string>();
        public static Dictionary<uint, string> UNIQUE_DICT = new Dictionary<uint, string>
        {
            { 2002, "Isyutaru" },
            { 1982, "Uruchi" },
            { 1954, "Tiger Girl" },
            { 3877, "Roc" },
            { 3875, "Demon Shaitan" },
            { 3810, "Lord Yarkan" },
            { 14997, "BeakYuang" },
            { 14936, "Captain Ivy" },
            { 5871, "Cerberus" },
            { 22890, "SoSo The Black Viper" }
        };

        // CTF / BATTLE ARENA STUFF
        public static List<string> event_chars = new List<string>();
        #endregion

        #region ADVANCED
        public static bool ANTI_JOB_TRACE = true;
        public static bool JOB_REVERSE_SCROLL = false;
        public static bool JOB_RES_SCROLL = true;
        public static bool ANTI_JOB_SKILL = false;
        public static bool JOB_REVERSE_DEATH_POINT = true;
        public static bool JOB_REVERSE_LAST_RECALL_POINT = true;
        public static bool JOB_REVERSE_MAP_POINT = true;
        public static bool ANTI_JOB_PET2 = false;
        public static bool ANTI_JOB_ZERK = false;
        public static bool ANTI_JOB_ZERK_POTION = false;
        public static bool ANTI_JOB_CHEAT = false;
        public static bool ANTI_JOB_TRADE = true;
        public static string ANTI_JOB_TRADE_START = "00:00:01";
        public static string ANTI_JOB_TRADE_STOP = "06:00:01";
        public static bool ANTI_SKILL = false;
        public static bool ANTI_FORTRESS_SKILL = false;
        public static bool FTW_TRACE = false;
        public static bool FTW_RES_SCROLL = false;
        public static bool ANTI_FORTRESS_ZERK = false;
        public static bool ANTI_FORTRESS_ZERK_POTION = false;
        #endregion
    }
}
