﻿namespace Silkroad.Framework.Configure
{
    public class RedirectionRule
    {
        public string Ip { get; set; }
        public ushort Port { get; set; }
    }
}
