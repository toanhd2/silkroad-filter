﻿using System.Collections.Generic;
using Silkroad.Framework.Enums;

namespace Silkroad.Framework.Configure
{
    public class ServerSettings
    {
        public string Name { get; set; }
        public ServerType Type { get; set; }
        public string PublicIp { get; set; }
        public ushort PublicPort { get; set; }
        public string Ip { get; set; }
        public ushort Port { get; set; }
        public List<RedirectionRule> RedirectionRules { get; set; }
    }
}
