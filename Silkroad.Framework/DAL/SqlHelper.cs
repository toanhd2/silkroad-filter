﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Data.SqlClient;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Models;

namespace Silkroad.Framework.DAL
{
    public static class SqlHelper
    {
        private static string _connectionStr;
        public static void Initial(string connectionStr)
        {
            _connectionStr = connectionStr;
        }

        /// <summary>
        /// Attempts to establish connection to the database.
        /// </summary>
        /// <returns>Boolean</returns>
        public static async Task<bool> CheckConnection()
        {
            await using SqlConnection connection = new SqlConnection(_connectionStr);
            try
            {
                await connection.OpenAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Constructs a SqlCommand with the given parameters. This method is normally called
        /// from the other methods and not called directly. But here it is if you need access
        /// to it.
        /// </summary>
        /// <param name="qry">SQL query or stored procedure name</param>
        /// <param name="connection">Current SQL connection</param>
        /// <param name="type">Type of SQL command</param>
        /// <param name="args">Query arguments. Arguments should be in pairs where one is the
        /// name of the parameter and the second is the value. The very last argument can
        /// optionally be a SqlParameter object for specifying a custom argument type</param>
        /// <returns></returns>
        public static SqlCommand CreateCommand(string qry, SqlConnection connection, CommandType type, params object[] args)
        {
            var cmd = new SqlCommand(qry, connection)
            {
                CommandType = type
            };

            if (args == null) return cmd;
            // Construct SQL parameters
            for (var i = 0; i < args.Length; i++)
            {
                switch (args[i])
                {
                    case string _ when i < (args.Length - 1):
                        var sqlParameter = new SqlParameter {ParameterName = (string) args[i], Value = args[++i]};
                        cmd.Parameters.Add(sqlParameter);
                        break;
                    case SqlParameter _:
                        cmd.Parameters.Add((SqlParameter)args[i]);
                        break;
                    default:
                        throw new ArgumentException("Invalid number or type of arguments supplied");
                }
            }
            return cmd;
        }

        /// <summary>
        /// Executes async query or name of stored procedure that returns no results
        /// </summary>
        /// <param name="query">Query or Name of stored procedure</param>
        /// <param name="commandType">Command type</param>
        /// <param name="args">Any number of parameter name/value pairs and/or SQLParameter arguments</param>
        /// <returns>The number of rows affected</returns>
        public static async Task<int> ExecNonQueryAsync(string query, CommandType commandType, params object[] args)
        {
            await using SqlConnection connection = new SqlConnection(_connectionStr);
            await using SqlCommand cmd = CreateCommand(query, connection, commandType, args);
            await connection.OpenAsync();
            return await cmd.ExecuteNonQueryAsync();
        }

        /// <summary>
        /// Executes query or name of stored procedure that returns no results
        /// </summary>
        /// <param name="query">Query or Name of stored procedure</param>
        /// <param name="commandType">Command type</param>
        /// <param name="args">Any number of parameter name/value pairs and/or SQLParameter arguments</param>
        /// <returns>The number of rows affected</returns>
        public static int ExecNonQuery(string query, CommandType commandType, params object[] args)
        {
            using SqlConnection connection = new SqlConnection(_connectionStr);
            using SqlCommand cmd = CreateCommand(query, connection, commandType, args);
            connection.Open();
            return cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Executes async a query that returns a single value
        /// </summary>
        /// <param name="query">Query text</param>
        /// <param name="commandType">Command type</param>
        /// <param name="args">Any number of parameter name/value pairs and/or SQLParameter arguments</param>
        /// <returns>Value of first column and first row of the results</returns>
        public static async Task<T> ExecScalarAsync<T>(string query, CommandType commandType, params object[] args)
        {
            await using SqlConnection connection = new SqlConnection(_connectionStr);
            await using SqlCommand cmd = CreateCommand(query, connection, commandType, args);
            await connection.OpenAsync();
            var result = await cmd.ExecuteScalarAsync();
            return result == DBNull.Value ? default : (T) result;
        }

        /// <summary>
        /// Executes a query that returns a single value
        /// </summary>
        /// <param name="query">Query text</param>
        /// <param name="commandType">Command type</param>
        /// <param name="args">Any number of parameter name/value pairs and/or SQLParameter arguments</param>
        /// <returns>Value of first column and first row of the results</returns>
        public static T ExecScalar<T>(string query, CommandType commandType, params object[] args)
        {
            using SqlConnection connection = new SqlConnection(_connectionStr);
            using SqlCommand cmd = CreateCommand(query, connection, commandType, args);
            connection.Open();
            var result = cmd.ExecuteScalar();
            return result == DBNull.Value ? default : (T)result;
        }

        /// <summary>
        /// Executes async a query and returns the SqlDataReader as results
        /// </summary>
        /// <param name="query">Query text</param>
        /// <param name="commandType">Command type</param>
        /// <param name="args">Any number of parameter name/value pairs and/or SQLParameter arguments</param>
        /// <returns>Results as a SqlDataReader</returns>
        public static async Task<SqlDataReader> ExecDataReaderAsync(string query, CommandType commandType, params object[] args)
        {
            await using SqlConnection connection = new SqlConnection(_connectionStr);
            await using SqlCommand cmd = CreateCommand(query, connection, commandType, args);
            await connection.OpenAsync();
            return await cmd.ExecuteReaderAsync();
        }

        /// <summary>
        /// Executes a query and returns the SqlDataReader as results
        /// </summary>
        /// <param name="query">Query text</param>
        /// <param name="commandType">Command type</param>
        /// <param name="args">Any number of parameter name/value pairs and/or SQLParameter arguments</param>
        /// <returns>Results as a SqlDataReader</returns>
        public static SqlDataReader ExecDataReader(string query, CommandType commandType, params object[] args)
        {
            using SqlConnection connection = new SqlConnection(_connectionStr);
            using SqlCommand cmd = CreateCommand(query, connection, commandType, args);
            connection.Open();
            return cmd.ExecuteReader();
        }

        /// <summary>
        /// Executes a query and returns the generic value as results
        /// </summary>
        /// <param name="query">Query text</param>
        /// <param name="commandType">Command type</param>
        /// <param name="args">Any number of parameter name/value pairs and/or SQLParameter arguments</param>
        /// <returns>Results as a SqlDataReader</returns>
        public static T QueryFirst<T>(string query, CommandType commandType, params object[] args)
        {
            using SqlConnection connection = new SqlConnection(_connectionStr);
            using SqlCommand cmd = CreateCommand(query, connection, commandType, args);
            connection.Open();
            var reader = cmd.ExecuteReader();
            return reader.Read() ? reader.Map<T>() : default;
        }

        /// <summary>
        /// Executes a query and returns the list generic values as results
        /// </summary>
        /// <param name="query">Query text</param>
        /// <param name="commandType">Command type</param>
        /// <param name="args">Any number of parameter name/value pairs and/or SQLParameter arguments</param>
        /// <returns>Results as a SqlDataReader</returns>
        public static IEnumerable<T> Query<T>(string query, CommandType commandType, params object[] args)
        {
            IList<T> result = new List<T>();
            using SqlConnection connection = new SqlConnection(_connectionStr);
            using SqlCommand cmd = CreateCommand(query, connection, commandType, args);
            connection.Open();
            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                result.Add(reader.Map<T>());
            }
            return result;
        }

        public static bool IsFellowPetSummonedByCharName(string charName)
        {
            var query = @"Select top (1) CanBeVehicle from " + Settings.SHA_DB + ".dbo._RefObjChar where CanBeVehicle = 1 and ID =(select top (1) Link from " + Settings.SHA_DB + ".dbo._RefObjCommon where ID = (select top (1) RefCharID from " + Settings.SHA_DB + ".dbo._CharCOS as A inner join " + Settings.SHA_DB + ".dbo._Items as B on A.ID = B.Data and A.ID != 0 inner join " + Settings.SHA_DB + ".dbo._Inventory as C on B.ID64 = C.ItemID inner join " + Settings.SHA_DB + ".dbo._Char as D on C.CharID = D.CharID where D.CHARNAME16 = '" + charName + "' and A.[State] = 3 order by A.RentEndTime))";

            using var connection = new SqlConnection(_connectionStr);
            byte? canBeVehicle = connection.QuerySingleOrDefault<byte>(query);
            return canBeVehicle == 1;
        }

        /// <summary>
        /// Get character info by character name
        /// </summary>
        /// <param name="charName"></param>
        /// <returns>Character info</returns>
        public static CharacterInfo GetCharacterInfoByCharName(string charName)
        {
            string query = @$"SELECT TOP(1) C.LatestRegion, J.JobType,
                            (CASE WHEN I.ItemID = 0 THEN 0 ELSE 1 END) AS JobState 
                            FROM [{Settings.SHA_DB}].[dbo].[_Char] AS C WITH(NOLOCK)
                            INNER JOIN [{Settings.SHA_DB}].[dbo].[_Inventory] AS I WITH(NOLOCK) ON I.CharID = C.CharID
                            INNER JOIN [{Settings.SHA_DB}].[dbo].[_CharTrijob] AS J WITH(NOLOCK) ON C.CharID = J.CharID 
                            WHERE I.Slot = 8 AND C.CharName16 = @CharName";

            using var connection = new SqlConnection(_connectionStr);
            return connection.QuerySingleOrDefault<CharacterInfo>(query, new { CharName = charName }, commandType: CommandType.Text);
        }

        /// <summary>
        /// Count guild members by member name
        /// </summary>
        /// <param name="charName"></param>
        /// <returns>Total members</returns>
        public static int CountGuildMemberByCharName(string charName)
        {
            string query = $@"SELECT COUNT(CharID) FROM [{Settings.SHA_DB}].[dbo].[_GuildMember] WHERE CharName = @CharName";
            using var connection = new SqlConnection(_connectionStr);
            return connection.QuerySingle<int>(query, new { CharName = charName }, commandType: CommandType.Text);
        }

        public static int CountUnionMemberByCharName(string charName)
        {
            string query = $@"SELECT TOP(1) CASE WHEN AC.Ally2 > 0 THEN 1 ELSE 0 END + CASE WHEN AC.Ally3 > 0 THEN 1 ELSE 0 END + 
                            CASE WHEN AC.Ally4 > 0 THEN 1 ELSE 0 END + CASE WHEN AC.Ally5 > 0 THEN 1 ELSE 0 END + 
                            CASE WHEN AC.Ally6 > 0 THEN 1 ELSE 0 END + CASE WHEN AC.Ally7 > 0 THEN 1 ELSE 0 END + 
                            CASE WHEN AC.Ally8 > 0 THEN 1 ELSE 0 END AS Total FROM [{Settings.SHA_DB}].[dbo].[_Guild] AS G WITH(NOLOCK)
                            JOIN [{Settings.SHA_DB}].[dbo].[_GuildMember] AS GM WITH(NOLOCK) ON GM.GuildID = G.ID
                            JOIN [{Settings.SHA_DB}].[dbo].[_AlliedClans] AS AC WITH(NOLOCK) ON G.Alliance = AC.ID
                            WHERE GM.CharName = @CharName";

            using var connection = new SqlConnection(_connectionStr);
            return connection.QuerySingleOrDefault<int>(query, new { CharName = charName }, commandType: CommandType.Text);
        }

        public static short GetLatestRegionByCharName(string charName)
        {
            string query = @$"SELECT TOP(1) LatestRegion FROM [{Settings.SHA_DB}].[dbo].[_Char] WITH(NOLOCK)";
            using var connection = new SqlConnection(_connectionStr);
            return connection.QuerySingle<short>(query, new { CharName = charName }, commandType: CommandType.Text);
        }

        public static byte? GetCurrentInventorySlotOfRepairHammer(string charName)
        {
            string query = @$"SELECT TOP(1) V.Slot FROM [{Settings.SHA_DB}].[dbo].[_Inventory] AS V
                            JOIN [{Settings.SHA_DB}].[dbo].[_Items] AS I ON I.ID64 = V.ItemID
                            JOIN [{Settings.SHA_DB}].[dbo].[_Char] AS c ON c.CharID = V.CharID
                            WHERE c.CharName16 = @CharName AND I.RefItemID IN (3784, 24458)";

            using var connection = new SqlConnection(_connectionStr);
            return connection.QuerySingleOrDefault<byte?>(query, new { CharName = charName }, commandType: CommandType.Text);
        }

        public static async Task<int> LogUniqueNoticeAsync(UniqueNotice noticeType, string killerName, string uniqueName)
        {
            string query = @$"INSERT INTO [{Settings.FILTER_DB}].[dbo].[_LogUnique]([KillerName], [UniqueName], [Type], [Date])
                            VALUES (@KillerName ,@UniqueName ,@Type ,@Time)";
            var param = new
            {
                KillerName = killerName,
                UniqueName = uniqueName,
                Type = noticeType.ToString(),
                Time = DateTime.Now
            };

            await using var connection = new SqlConnection(_connectionStr);
            return await connection.ExecuteAsync(query, param, commandType: CommandType.Text);
        }

        public static async Task<int> LogChatAsync(ChatType chatType, string sender, string content)
        {
            string query = @$"INSERT INTO [{Settings.FILTER_DB}].[dbo].[_LogChat]([Sender], [Content], [Type], [Date])
                            VALUES (@Sender ,@Content ,@Type ,@Time)";
            var param = new
            {
                Sender = sender,
                Content = content,
                Type = chatType.ToString(),
                Time = DateTime.Now
            };

            await using var connection = new SqlConnection(_connectionStr);
            return await connection.ExecuteAsync(query, param, commandType: CommandType.Text);
        }

        public static async Task<string> GetItemNameByRefItemIDAsync(uint refItemId)
        {
            string query = @$"SELECT TOP(1) I.Name FROM [{Settings.FILTER_DB}].[dbo].[_ItemName] AS I
                            JOIN [{Settings.SHA_DB}].[dbo].[_RefObjCommon] AS O
                            ON I.CodeName128 COLLATE Database_Default = O.NameStrID128 COLLATE DATABASE_DEFAULT
                            WHERE O.ID = @Id";

            await using var connection = new SqlConnection(_connectionStr);
            return await connection.QuerySingleOrDefaultAsync<string>(query, new { Id = (int)refItemId }, commandType: CommandType.Text);
        }

        public static async Task<int> GetAdvancedElixirValueAsync(string charName, byte slot)
        {
            string query = @$"SELECT TOP(1) B.nOptValue FROM {Settings.SHA_DB}.dbo._BindingOptionWithItem AS B
                            JOIN {Settings.SHA_DB}.dbo._Inventory AS I ON B.nItemDBID = I.ItemID
                            JOIN {Settings.SHA_DB}.dbo._Char AS C ON I.CharID = C.CharID
                            WHERE c.CharName16 = @CharName AND I.Slot = @Slot";
            var param = new
            {
                CharName = charName,
                Slot = slot
            };

            await using var connection = new SqlConnection(_connectionStr);
            return await connection.QuerySingleOrDefaultAsync<int>(query, param, commandType: CommandType.Text);
        }
    }
}
