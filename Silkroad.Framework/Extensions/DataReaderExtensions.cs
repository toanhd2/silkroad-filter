﻿using System;
using System.Data;
using System.Reflection;

namespace Silkroad.Framework.Extensions
{
    public static class DataReaderExtensions
    {
        public static T Map<T>(this IDataReader dataReader)
        {
            T obj = Activator.CreateInstance<T>();
            PropertyInfo[] props = typeof(T).GetProperties();
            foreach (var prop in props)
            {
                object value = dataReader[prop.Name];
                if (value == null || DBNull.Value.Equals(value))
                {
                    prop.SetValue(obj, default(T));
                    continue;
                }
                Type type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
                prop.SetValue(obj, Convert.ChangeType(value, type));
            }
            return obj;
        }
    }
}
