﻿using Silkroad.Framework.Security;
using System;
using System.Collections.Generic;

namespace Silkroad.Framework.Extensions
{
    /// <summary>
    /// Thanks to Daxters SilkroadPacketApi :).
    /// </summary>
    public static class PacketExtensions
    {
        //--------------------------------------------------------------------------
        #region Reader / writer delegates & method defs

        private delegate dynamic PacketReadHandler(Packet packet);
        private delegate void PacketWriteHandler(Packet packet, dynamic value);

        /// <summary>
        /// Types that are available for reading as enums.
        /// </summary>
        private static readonly Dictionary<Type, PacketReadHandler> _enumReaders =
            new Dictionary<Type, PacketReadHandler>()
            {
                { typeof(byte), reader => reader.ReadValue<byte>() },
                { typeof(sbyte), reader => reader.ReadValue<sbyte>() },
                { typeof(ushort), reader => reader.ReadValue<ushort>() },
                { typeof(short), reader => reader.ReadValue<short>() },
                { typeof(uint), reader => reader.ReadValue<uint>() },
                { typeof(int), reader => reader.ReadValue<int>() },
                { typeof(ulong), reader => reader.ReadValue<ulong>() },
                { typeof(long), reader=> reader.ReadValue<long>() }
            };

        /// <summary>
        /// Types that are available for writing as enums.
        /// </summary>
        private static readonly Dictionary<Type, PacketWriteHandler> _enumWriters =
            new Dictionary<Type, PacketWriteHandler>()
            {
                { typeof(byte), (packet, value) => packet.WriteValue<byte>(value) },
                { typeof(sbyte), (packet, value) => packet.WriteValue<sbyte>(value) },
                { typeof(ushort), (packet, value) => packet.WriteValue<ushort>(value) },
                { typeof(short), (packet, value) => packet.WriteValue<short>(value) },
                { typeof(uint), (packet, value) => packet.WriteValue<uint>(value) },
                { typeof(int), (packet, value) => packet.WriteValue<int>(value) },
                { typeof(ulong), (packet, value) => packet.WriteValue<ulong>(value) },
                { typeof(long), (packet, value) => packet.WriteValue<long>(value) }
            };

        #endregion

        //--------------------------------------------------------------------------

        #region Readers

        /// <summary>
        /// Reads enum with given underlying type. See _enumReaders for supported types.
        /// </summary>
        /// <typeparam name="TEnum">The enum type.</typeparam>
        /// <param name="packet">The packet instance.</param>
        /// <returns>Readen value.</returns>
        public static TEnum ReadEnum<TEnum>(this Packet packet) where
            TEnum : struct, IComparable, IFormattable, IConvertible
        {
            if (!typeof(TEnum).IsEnum)
                throw new ArgumentException("Given type is not enumaration.");

            //TODO: Check the type name output !
            if (!_enumReaders.TryGetValue(Enum.GetUnderlyingType(typeof(TEnum)), out PacketReadHandler reader))
                throw new ArgumentException($"Reader for type {typeof(TEnum).Name} does not exist.");

            return (TEnum)reader.Invoke(packet);
        }

        #endregion

        //--------------------------------------------------------------------------

        #region Writers

        /// <summary>
        /// Writes enum with given underlying type. See _enumWriters for supported types.
        /// </summary>
        /// <typeparam name="TEnum">The enum type.</typeparam>
        /// <param name="packet">The packet instance.</param>
        /// <param name="enum">The enum.</param>
        public static void WriteEnum<TEnum>(this Packet packet, TEnum @enum) where
            TEnum : struct, IComparable, IFormattable, IConvertible
        {
            if (!typeof(TEnum).IsEnum)
                throw new ArgumentException($"Given type {typeof(TEnum).Name} is not enumeration.");

            if (!_enumWriters.TryGetValue(Enum.GetUnderlyingType(typeof(TEnum)), out PacketWriteHandler writer))
                throw new ArgumentException($"Writer for type {typeof(TEnum).Name} does not exist.");

            writer(packet, @enum);
        }

        #endregion

        //--------------------------------------------------------------------------
    }
}
