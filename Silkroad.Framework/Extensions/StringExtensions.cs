﻿using System;
namespace Silkroad.Framework.Extensions
{
    public static class StringExtensions
    {
        public static string ValidateSqlInjection(this string str)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            return str.Replace("'", string.Empty)
                .Replace(";", string.Empty)
                .Replace("\\", string.Empty)
                .Replace("\"", string.Empty);
        }

        public static string ValidateCharacterName(this string str)
        {
            if (str is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            return str.Replace("'", string.Empty)
                .Replace(";", string.Empty)
                .Replace("-", string.Empty)
                .Replace("\\", string.Empty)
                .Replace("\"", string.Empty)
                .Replace("[", "%[%")
                .Replace("]", "%]%");
        }
    }
}
