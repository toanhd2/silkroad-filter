﻿namespace Silkroad.Framework.Enums
{
    public enum ExitActionType : byte
    {
        Exit = 0x0001,
        Restart = 0x0002
    }
}
