﻿namespace Silkroad.Framework.Enums
{
    public enum GMCommand : ushort
    {
        ToTown = 3,
        LoadMonster = 6,
        MakeItem = 7,
        MoveToUser = 8,
        Zoe = 12,
        Ban = 13,
        Invisible = 14,
        Invincible = 15,
        RecallUser = 17,
        RecallGuild = 18,
        MobKill = 20,
        SpawnUniqueLoc = 42
    }
}
