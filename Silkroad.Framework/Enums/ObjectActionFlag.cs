﻿namespace Silkroad.Framework.Enums
{
    public enum ObjectActionFlag : byte
    {
        Start = 1,
        Cancel = 2
    }
}
