﻿namespace Silkroad.Framework.Enums
{
    public enum StatusType : byte
    {
        LifeState = 0,
        MotionState = 1,
        //Game state
        GameState = 4,
        //Pvp state
        PVPState = 7,
        //Combat
        CombatState = 8,
        //Use scroll
        ScrollingState = 11
    }
}
