﻿namespace Silkroad.Framework.Enums
{
    public enum AutoInverstExp : byte
    {
        None = 0,
        Beginner = 1,
        Helpful = 2,
        BeginnerAndHelpful = 3
    }
}
