﻿namespace Silkroad.Framework.Enums
{
    public enum ServerType : byte
    {
        None = 0,
        GatewayServer = 1,
        AgentServer = 2,
        DownloadServer = 3,
    }
}
