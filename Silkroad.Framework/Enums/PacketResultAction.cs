﻿namespace Silkroad.Framework.Enums
{
    public enum PacketResultAction
    {
        None,
        Ignore,
        Disconnect,
        Replace,
        Response
    }
}
