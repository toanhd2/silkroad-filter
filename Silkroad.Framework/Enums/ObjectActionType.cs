﻿namespace Silkroad.Framework.Enums
{
    public enum ObjectActionType : byte
    {
        NormalAttack = 1,
        ItemPick = 2,
        Trace = 3,
        UseSkill = 4,
    }
}
