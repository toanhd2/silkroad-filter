﻿namespace Silkroad.Framework.Enums
{
    public enum SiegeActionType : byte
    {
        TaxRateChange = 1,
        RegistrationFortressWar = 7,
        AboutGuildChange = 26
    }
}
