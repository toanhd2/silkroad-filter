﻿namespace Silkroad.Framework.Enums
{
    public enum PVPMode : byte
    {
        None = 0,

        /// <summary>
        /// Team Red Hawk
        /// </summary>
        Red = 1,

        /// <summary>
        /// Team Black Turtle
        /// </summary>
        Black = 2,

        /// <summary>
        /// Team Blue Dragon
        /// </summary>
        Blue = 3,

        /// <summary>
        /// Team White Tiger
        /// </summary>
        White = 4,

        /// <summary>
        /// Team Giraffe
        /// </summary>
        Yellow = 5,
    }
}
