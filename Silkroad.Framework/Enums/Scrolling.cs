﻿namespace Silkroad.Framework.Enums
{
    public enum Scrolling
	{
		None = 0,
		ReturnScroll = 1,
		BanditReturnScroll = 2
	}
}
