﻿namespace Silkroad.Framework.Enums
{
    public enum ReverseType : byte
    {
        LastRecallPoint = 2,
        DeathPoint = 3,
        MapPoint = 7
    }
}
