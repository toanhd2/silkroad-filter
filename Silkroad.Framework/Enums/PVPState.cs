﻿namespace Silkroad.Framework.Enums
{
    public enum PVPState : byte
	{
		Neutral = 0,
		Assaulter = 1,
		PlayerKiller = 2
	}
}
