﻿namespace Silkroad.Framework.Enums
{
    public enum ItemActionType : byte
    {
        Equip = 0,
        Drop = 7,
        Buy = 8,
        Sell = 9,
        MoveToPet = 27,
        MoveToGuildStorage = 30
    }
}
