﻿namespace Silkroad.Framework.Enums
{
    public enum FortressType : byte
    {
        Jangan = 1,
        Bandit = 3,
        Hotan = 6
    }
}
