﻿namespace Silkroad.Framework.Enums
{
    public enum GameState : byte
	{
		None = 0,
		Berserk = 1,
		Untouchable = 2,
		GameMasterInvincible = 3,
		GameMasterUntouchable = 4,
		GameMasterInvisible = 5,
		Stealth = 6,
		Invisible = 7
	}
}
