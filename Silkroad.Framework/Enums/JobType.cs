﻿namespace Silkroad.Framework.Enums
{
    public enum JobType
    {
        None = 0,
        Trader = 1,
        Thief = 2,
        Hunter = 3
    }
}
