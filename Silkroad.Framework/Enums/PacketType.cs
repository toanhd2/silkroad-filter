﻿using System;

namespace Silkroad.Framework.Enums
{
    /// <summary>
    /// The packet source.
    /// </summary>
    [Flags]
    public enum PacketType
    {
        Server,
        Client,
    }
}
