﻿namespace Silkroad.Framework.Enums
{
    public enum LifeState : byte
    {
        Unknow = 0,
        Alive = 1,
        Dead = 2
    }
}
