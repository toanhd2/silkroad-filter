﻿namespace Silkroad.Framework.Enums
{
    public enum CastSkillErrorType : byte
    {
        InvalidTarget = 6,
        SkillMissMatchWeapon = 13,
        ArrowNotEnough = 14,
        BrokenWeapon = 15,
        InsideTown = 24
    }
}
