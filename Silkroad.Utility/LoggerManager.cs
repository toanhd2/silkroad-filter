﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;

namespace Silkroad.Utility
{
    public static class LoggerManager
    {
        private static ILoggerFactory _loggerFactory;
        public static void Initialize(IConfiguration configuration)
        {
            // Initialize serilog logger
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();

            _loggerFactory = new LoggerFactory();
            _loggerFactory.AddSerilog(Log.Logger);
        }

        public static ILogger<T> GetInstance<T>()
        {
            return _loggerFactory.CreateLogger<T>();
        }
    }
}
