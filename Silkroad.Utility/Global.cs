﻿using System.Collections.Concurrent;

namespace Silkroad.Utility
{
    public static class Global
    {
        // Store token-hardwareId as key value pair
        public static ConcurrentDictionary<uint, string> SessionIdentitys = new ConcurrentDictionary<uint, string>();

        public static string GetHardwareIdByToken(uint token)
        {
            SessionIdentitys.TryGetValue(token, out string hardwareId);
            return hardwareId;
        }

        public static void StoreHardwareId(uint token, string hardwareId)
        {
            SessionIdentitys.AddOrUpdate(token, hardwareId, (key, value) => hardwareId);
        }
    }
}
