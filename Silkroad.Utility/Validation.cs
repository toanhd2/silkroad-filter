﻿using System.Text.RegularExpressions;

namespace Silkroad.Utility
{
    public static class Validation
    {
        public static bool HasSpecialCharacter(string str)
        {
            const string regex = @"\""|\-|\'|\\";
            return Regex.IsMatch(str, regex);
        }
    }
}
