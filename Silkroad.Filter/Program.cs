﻿using Microsoft.Extensions.Configuration;
using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Gateway.Network;
using Silkroad.Utility;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Serilog;
using Silkroad.Framework.DAL;

namespace Silkroad.Filter
{
    public class Program
    {
        public static IConfigurationRoot Configuration;
        private static void Main(string[] args)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            ConfigureServices();
            LoggerManager.Initialize(Configuration);

            // Print connection string to demonstrate configuration object is populated
            var connectionString = Configuration.GetConnectionString("DataConnection");
            SqlHelper.Initial(connectionString);
            var gatewaySetting = Configuration.GetSection("GatewaySettings").Get<ServerSettings>();
            var gatewayServer = new GatewayServer(gatewaySetting);
            gatewayServer.Start();
            var agentSetting = Configuration.GetSection("AgentSettings").Get<ServerSettings>();
            var agentServer = new AgentServer(agentSetting);
            agentServer.Start();
            stopwatch.Stop();
            Log.Information($"Total times: {stopwatch.ElapsedMilliseconds} ms");
            //var _ = new Timer(CountThread, null, 0, 30000);

            // Perform text input
            for (; ; )
            {
                string line = Console.ReadLine();
                if (string.IsNullOrEmpty(line))
                    break;
                Console.WriteLine();
            }
        }

        private static void CountThread(object state)
        {
            var count = Process.GetCurrentProcess().Threads.Count;
            Console.WriteLine($"Total Threads: {count}");
            using Process proc = Process.GetCurrentProcess();
            Console.WriteLine($"Total Ram: {proc.PrivateMemorySize64 / (1024 * 1024)}");
        }

        private static void ConfigureServices()
        {
            // Build Configuration
            Configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .Build();
        }
    }
}
