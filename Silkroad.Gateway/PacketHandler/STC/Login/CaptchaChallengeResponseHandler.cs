﻿using Silkroad.Framework.Configure;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using Silkroad.Gateway.Network;

namespace Silkroad.Gateway.PacketHandler.STC.Login
{
    /// <summary>
    /// S->C SERVER_GATEWAY_CAPTCHA_CHALLENGE = 0x2322
    /// </summary>
    public class CaptchaChallengeResponseHandler : IPacketHandler<GatewaySession>
    {
        public PacketResult Handle(GatewaySession session, Packet packet)
        {
            if (!Settings.DISABLE_CAPTCHA) return PacketResult.None;

            Packet captchaPacket = new Packet(0x6323);
            captchaPacket.WriteValue<string>(Settings.CAPTCHA_CHAR);
            return new PacketResult(PacketResultAction.Response, captchaPacket);
        }
    }
}
