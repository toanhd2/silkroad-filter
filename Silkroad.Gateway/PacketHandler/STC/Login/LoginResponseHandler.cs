﻿using Silkroad.Framework.Enums;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using Silkroad.Gateway.Network;
using System.Linq;

namespace Silkroad.Gateway.PacketHandler.STC.Login
{
    /// <summary>
    /// S->C SERVER_GATEWAY_LOGIN_RESPONSE = 0xA102
    /// </summary>
    public class LoginResponseHandler : IPacketHandler<GatewaySession>
    {
        public PacketResult Handle(GatewaySession session, Packet packet)
        {
            var resultCode = packet.ReadEnum<ResultCode>();
            if (resultCode != ResultCode.Success) return PacketResult.None;

            var id = packet.ReadValue<uint>();
            Packet replacePacket = new Packet(0xA102, true);
            replacePacket.WriteValue<byte>(0x01);
            replacePacket.WriteValue<uint>(id);
            replacePacket.WriteValue<string>(session.Server.Settings.RedirectionRules.First().Ip);
            replacePacket.WriteValue<ushort>(session.Server.Settings.RedirectionRules.First().Port);
            replacePacket.WriteValue<uint>(0);
            replacePacket.Lock();
            return new PacketResult(PacketResultAction.Replace, replacePacket);
        }
    }
}
