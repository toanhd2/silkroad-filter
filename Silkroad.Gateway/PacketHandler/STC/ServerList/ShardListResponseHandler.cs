﻿using Silkroad.Framework.Configure;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using Silkroad.Gateway.Network;

namespace Silkroad.Gateway.PacketHandler.STC.ServerList
{
    /// <summary>
    /// S->C SERVER_GATEWAY_SHARD_LIST_RESPONSE = 0xA101
    /// </summary>
    public class ShardListResponseHandler : IPacketHandler<GatewaySession>
    {
        public PacketResult Handle(GatewaySession session, Packet packet)
        {
            if (Settings.FAKE_PLAYERS == 0 && 
                string.IsNullOrWhiteSpace(Settings.SERVER_NAME))
            {
                return PacketResult.None;
            }

			Packet shardListResponse = new Packet(0xA101, true);
			byte flag = packet.ReadValue<byte>();
			shardListResponse.WriteValue<byte>(flag);
			if (flag == 0x01)
			{
				// Read stuff
				var globalOperationType = packet.ReadValue<byte>();
				var globalOperationName = packet.ReadValue<string>();
				// New flag
				flag = packet.ReadValue<byte>();
				shardListResponse.WriteValue<byte>(globalOperationType);
				shardListResponse.WriteValue<string>(globalOperationName);
				shardListResponse.WriteValue<byte>(flag);
			}

			flag = packet.ReadValue<byte>();
			shardListResponse.WriteValue<byte>(flag);

			if (flag == 0x01)
			{
				packet.ReadValue<ushort>(); // Shard ID
				packet.ReadValue<string>(); // Shard Name
				var shardPlayers = packet.ReadValue<ushort>(); // Current players
				packet.ReadValue<ushort>(); // Capacity
				/*
					1 = ONLINE
					0 = CHECK
				*/
				var status = packet.ReadValue<byte>();
				var globalOperationId = packet.ReadValue<byte>();
				flag = packet.ReadValue<byte>();

				var fakePlayer = shardPlayers + Settings.FAKE_PLAYERS;
                if (fakePlayer > Settings.MAX_PLAYERS)
                {
					fakePlayer = Settings.MAX_PLAYERS;
                }
				shardListResponse.WriteValue<ushort>(Settings.SHARD_ID);
				shardListResponse.WriteValue<string>(Settings.SERVER_NAME);
				shardListResponse.WriteValue<ushort>((ushort)fakePlayer);
				shardListResponse.WriteValue<ushort>(Settings.MAX_PLAYERS);
				shardListResponse.WriteValue<byte>(status);
				shardListResponse.WriteValue<byte>(globalOperationId);
				shardListResponse.WriteValue<byte>(flag);
			}
			
            return new PacketResult(PacketResultAction.Replace, shardListResponse);
        }
    }
}
