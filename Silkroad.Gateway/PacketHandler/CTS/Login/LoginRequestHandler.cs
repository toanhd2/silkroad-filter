﻿using Silkroad.Framework.Configure;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using Silkroad.Gateway.Network;

namespace Silkroad.Gateway.PacketHandler.CTS.Login
{
    /// <summary>
    /// C->S CLIENT_GATEWAY_LOGIN_REQUEST = 0x6102
    /// </summary>
    public class LoginRequestHandler : IPacketHandler<GatewaySession>
    {
        public PacketResult Handle(GatewaySession session, Packet packet)
        {
            // Anti exploit(gateway)
            //if (!session.Context.HaveRequestPatch ||
            //    !session.Context.HaveRequestServerList)
            //{
            //    return PacketResult.Disconnect;
            //}

            packet.ReadEnum<SrOperationType>();
            session.Context.UserName = packet.ReadValue<string>()?.ValidateSqlInjection();
            if (Settings.GM_LOGIN &&
                !Settings.GM_ACCOUNTS.Contains(session.Context.UserName))
            {
                return PacketResult.Disconnect;
            }
            
            return PacketResult.None;
        }
    }
}
