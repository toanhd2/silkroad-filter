﻿using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using Silkroad.Gateway.Network;

namespace Silkroad.Gateway.PacketHandler.CTS
{
    /// <summary>
    /// C->S CLIENT_PING_REQUEST = 0x2002
    /// </summary>
    public class PingRequestHandler : IPacketHandler<GatewaySession>
    {

        public PacketResult Handle(GatewaySession session, Packet packet)
        {
            return packet.Length == 0 ? PacketResult.None : PacketResult.Disconnect;
        }
    }
}
