﻿using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using Silkroad.Gateway.Network;

namespace Silkroad.Gateway.PacketHandler.CTS.Launcher
{
    /// <summary>
    /// C->S CLIENT_GATEWAY_PATCH_REQUEST = 0x6100, Encrypted
    /// </summary>
    public class PatchRequestHandler : IPacketHandler<GatewaySession>
    {
        public PacketResult Handle(GatewaySession session, Packet packet)
        {
            session.Context.HaveRequestPatch = true;
            return PacketResult.None;
        }
    }
}
