﻿using Silkroad.Framework.Configure;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using Silkroad.Gateway.Network;

namespace Silkroad.Gateway.PacketHandler.CTS.ServerList
{
    /// <summary>
    /// C->S CLIENT_GATEWAY_SHARD_LIST_REQUEST = 0x6101, Encrypted
    /// TESTME
    /// </summary>
    public class ServerListRequestHandler : IPacketHandler<GatewaySession>
    {
        public PacketResult Handle(GatewaySession session, Packet packet)
        {
            if (packet.Length != 0 || Settings.BLOCK_STATUS)
            {
                return PacketResult.Disconnect;
            }

            session.Context.HaveRequestServerList = true;

            return PacketResult.None;
        }
    }
}
