﻿namespace Silkroad.Gateway.Network
{
    public class GatewayContext
    {
        public string UserName { get; set; }
        public bool HaveRequestServerList { get; set; } = false;
        public bool HaveRequestPatch { get; set; } = false;

        public GatewayContext()
        {
        }
    }
}
