﻿using Microsoft.Extensions.Logging;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Network;
using Silkroad.Utility;
using System;

namespace Silkroad.Gateway.Network
{
    public class GatewaySession : Session
    {
        private readonly ILogger<GatewaySession> _log = LoggerManager.GetInstance<GatewaySession>();
        public new GatewayServer Server { get; set; }
        public GatewayContext Context { get; set; } = new GatewayContext();
        public GatewaySession(GatewayServer gatewayServer) : base(gatewayServer)
        {
            Server = gatewayServer;
        }

        public override void OnReceivedFromClient(byte[] buffer, int offset, int size)
        {
            try
            {
                ClientSecurity.Recv(buffer, 0, size);
                var packets = ClientSecurity.TransferIncoming();
                if (packets == null) return;
                foreach (var packet in packets)
                {
                    if (packet.Opcode == 0x5000 || packet.Opcode == 0x9000 || packet.Opcode == 0x2001)
                        continue;

                    if (packet.Opcode == 0x1608)
                    {
                        var hwid = packet.ReadValue<string>();
                        var mac = packet.ReadValue<string>();
                        _log.LogInformation($"HWID: {hwid}, Mac: {mac}");
                        continue;
                    }

                    var result = Server.PacketManager.Handle(PacketType.Client, this, packet);
                    switch (result.Action)
                    {
                        case PacketResultAction.Ignore:
                            continue;

                        case PacketResultAction.Disconnect:
                            Disconnect();
                            return;

                        case PacketResultAction.Replace:
                            foreach (var replacedPacket in result)
                                RemoteSecurity.Send(replacedPacket);
                            continue;

                        case PacketResultAction.Response:
                            foreach (var replacedPacket in result)
                                ClientSecurity.Send(replacedPacket);
                            TransferToClientAsync();
                            continue;
                    }

                    RemoteSecurity.Send(packet);
                }
                TransferToServerAsync();
            }
            catch (Exception ex)
            {
                _log.LogError(ex, ex.Message);
            }
        }

        public override void OnReceivedFromServer(byte[] buffer, int offset, int size)
        {
            try
            {
                RemoteSecurity.Recv(buffer, 0, size);
                var packets = RemoteSecurity.TransferIncoming();
                if (packets == null) return;
                foreach (var packet in packets)
                {
                    if (packet.Opcode == 0x5000 || packet.Opcode == 0x9000)
                        continue;
                    var result = Server.PacketManager.Handle(PacketType.Server, this, packet);
                    switch (result.Action)
                    {
                        case PacketResultAction.Ignore:
                            continue;

                        case PacketResultAction.Disconnect:
                            Disconnect();
                            return;

                        case PacketResultAction.Replace:
                            foreach (var replacedPacket in result)
                                ClientSecurity.Send(replacedPacket);
                            continue;

                        case PacketResultAction.Response:
                            foreach (var replacedPacket in result)
                                RemoteSecurity.Send(replacedPacket);
                            TransferToServerAsync();
                            continue;
                    }

                    ClientSecurity.Send(packet);
                }
                TransferToClientAsync();
            }
            catch (Exception ex)
            {
                _log.LogError(ex, ex.Message);
            }
        }
    }
}
