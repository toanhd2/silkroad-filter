﻿using Silkroad.Framework.Configure;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Network;
using Silkroad.Framework.Security;
using Silkroad.Gateway.PacketHandler.CTS;
using Silkroad.Gateway.PacketHandler.CTS.Launcher;
using Silkroad.Gateway.PacketHandler.CTS.Login;
using Silkroad.Gateway.PacketHandler.CTS.ServerList;
using Silkroad.Gateway.PacketHandler.STC.Login;
using Silkroad.Gateway.PacketHandler.STC.ServerList;

namespace Silkroad.Gateway.Network
{
    public class GatewayServer : Server
    {
        public PacketManager<GatewaySession> PacketManager { get; set; } = new PacketManager<GatewaySession>();
        public GatewayServer(ServerSettings serverSettings) : base(serverSettings)
        {
        }

        protected override Session CreateSession()
        {
            return new GatewaySession(this);
        }

        public override void SetupPacketHandler()
        {
            #region Client -> Server
            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_PING_REQUEST,
                new PingRequestHandler());

            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_GATEWAY_LOGIN_REQUEST,
                new LoginRequestHandler());

            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_GATEWAY_PATCH_REQUEST,
                new PatchRequestHandler());

            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_GATEWAY_SERVERLIST_REQUEST,
                new ServerListRequestHandler());
            #endregion

            #region Server -> Client
            PacketManager.RegisterHandler(
                PacketType.Server,
                Opcode.SERVER_GATEWAY_CAPTCHA_CHALLENGE,
                new CaptchaChallengeResponseHandler());

            PacketManager.RegisterHandler(
                PacketType.Server,
                Opcode.SERVER_GATEWAY_LOGIN_RESPONSE,
                new LoginResponseHandler());

            PacketManager.RegisterHandler(
                PacketType.Server,
                Opcode.SERVER_GATEWAY_SHARD_LIST_RESPONSE,
                new ShardListResponseHandler());
            #endregion
        }
    }
}
