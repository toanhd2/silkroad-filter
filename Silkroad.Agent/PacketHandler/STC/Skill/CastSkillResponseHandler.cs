﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Constants;
using Silkroad.Framework.DAL;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using System;

namespace Silkroad.Agent.PacketHandler.STC.Skill
{
    /// <summary>
    /// S->C SERVER_AGENT_CAST_SKILL_RESPONSE = 0xB070
    /// </summary>
    public class CastSkillResponseHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            // Currently only handle this packet for automatically to repair item
            if (!Settings.AUTO_REPAIR_ITEM) return PacketResult.None;

            ResultCode result = packet.ReadEnum<ResultCode>();
            if (result == ResultCode.Error)
            {
                CastSkillErrorType errorType = packet.ReadEnum<CastSkillErrorType>();
                if (errorType == CastSkillErrorType.BrokenWeapon)
                {
                    // Only do this every 5 minutes as it affects performance
                    var remainingTime = DateTime.Now.Subtract(session.Context.LastRepairItemTime).TotalMinutes;
                    if (remainingTime < 5)
                    {
                        return PacketResult.None;
                    }

                    byte? slot = SqlHelper.GetCurrentInventorySlotOfRepairHammer(session.Context.CharName);
                    if (slot.HasValue)
                    {
                        Packet repair = new Packet(Opcode.CLIENT_AGENT_PLAYER_HANDLE_REQUEST, true);
                        repair.WriteValue<byte>(slot);
                        repair.WriteValue<byte>(0xED);
                        repair.WriteValue<byte>(0x3E);

                        session.SendNotice(NoticeCode.AUTO_REPAIR_ITEM);
                        return new PacketResult(PacketResultAction.Response, repair);
                    }

                    session.Context.LastRepairItemTime = DateTime.Now;
                }
            }

            return PacketResult.None;
        }
    }
}
