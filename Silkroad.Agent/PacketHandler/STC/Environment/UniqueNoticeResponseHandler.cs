﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.DAL;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using System.Threading.Tasks;

namespace Silkroad.Agent.PacketHandler.STC.Environment
{
    /// <summary>
    /// S->C SERVER_NOTICE_UNIQUE_UPDATE = 0x300C
    /// </summary>
    public class UniqueNoticeResponseHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            if (Settings.LOG_UNIQUE)
            {
                var type = packet.ReadEnum<UniqueNotice>();
                switch (type)
                { 
                    case UniqueNotice.Spawn:
                        {
                            packet.ReadValue<byte>(); // Ukn
                            uint uniqueId = packet.ReadValue<uint>();
                            Settings.UNIQUE_DICT.TryGetValue(uniqueId, out string uniqueName);
                            //TODO: Remove duplicate log
                            Task.Run(() => SqlHelper.LogUniqueNoticeAsync(type, string.Empty, uniqueName ?? "Undefined"));
                        }
                        break;

                    case UniqueNotice.Killed:
                        {
                            packet.ReadValue<byte>(); // Ukn
                            uint uniqueId = packet.ReadValue<uint>();
                            string killerName = packet.ReadValue<string>();
                            if (session.Context.CharName == killerName)
                            {
                                Settings.UNIQUE_DICT.TryGetValue(uniqueId, out string uniqueName);
                                Task.Run(() => SqlHelper.LogUniqueNoticeAsync(type, killerName, uniqueName ?? "Undefined"));
                            }
                        }
                        break;
                }
            }
            return PacketResult.None;
        }
    }
}
