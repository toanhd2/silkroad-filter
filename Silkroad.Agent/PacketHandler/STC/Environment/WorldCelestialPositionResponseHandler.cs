﻿using Silkroad.Agent.Network;
using Silkroad.Framework.DAL;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Models;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.STC.Environment
{
    /// <summary>
    /// S->C SERVER_AGENT_ENVIRONMENT_CELESTIAL_POSITION = 0x3020
    /// </summary>
    public class WorldCelestialPositionResponseHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            CharacterInfo characterInfo = SqlHelper.GetCharacterInfoByCharName(session.Context.CharName);
            session.Context.UpdateStatusFromCharInfo(characterInfo);

            session.Context.BlockStall = false;
            session.Context.UniqueId = packet.ReadValue<uint>();
            return PacketResult.None;
        }
    }

}
