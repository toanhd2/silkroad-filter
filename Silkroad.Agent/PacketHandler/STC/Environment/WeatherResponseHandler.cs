﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.STC.Environment
{
    /// <summary>
    /// S->C SERVER_WEATHER_RESPONSE = 0xB50E
    /// </summary>
    public class WeatherResponseHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            session.Context.CharScreen = false;

            #region GM_START_VISIBLE
            if (Settings.GM_ANTI_INVISIBLE &&
                session.Context.GameState == GameState.GameMasterUntouchable)
            {
                Packet invincible = new Packet(Opcode.CLIENT_AGENT_GM_COMMAND_REQUEST);
                invincible.WriteValue<ushort>(GMCommand.Invisible);
                session.SendAsync(PacketType.Server, invincible);
            }
            #endregion

            return PacketResult.None;
        }
    }
}
