﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.STC.Character
{
    /// <summary>
    /// S->C SERVER_AGENT_CHARACTER_INFO_DATA  = 0x3013
    /// </summary>
    public class CharacterDataResponseHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
			packet.ReadValue<uint>();//Server time
			packet.ReadValue<uint>();//RefObjID
			packet.ReadValue<byte>();//Scale
			session.Context.CurLevel = packet.ReadValue<byte>();
			packet.ReadValue<byte>();//MaxLevel
			packet.ReadValue<ulong>();//ExpOffset
			packet.ReadValue<uint>();//SExpOffset
			packet.ReadValue<ulong>();//RemainGold
			packet.ReadValue<uint>();//RemainSkillPoint
			packet.ReadValue<ushort>();//RemainStatPoint
			packet.ReadValue<byte>();//RemainHwanCount
			packet.ReadValue<uint>();//GatheredExpPoint
			packet.ReadValue<uint>();//HP
			packet.ReadValue<uint>();//MP
			session.Context.AutoInverstExp = packet.ReadEnum<AutoInverstExp>();
			return PacketResult.None;
        }
    }
}
