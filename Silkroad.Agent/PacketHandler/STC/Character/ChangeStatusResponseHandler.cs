﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.STC.Character
{
	/// <summary>
	/// S->C SERVER_AGENT_CHANGE_STATUS_RESPONSE  = 0x30BF
	/// </summary>
	public class ChangeStatusResponseHandler : IPacketHandler<AgentSession>
	{
		public PacketResult Handle(AgentSession session, Packet packet)
		{
			uint uniqueId = packet.ReadValue<uint>();
            if (uniqueId == session.Context.UniqueId)
            {
				StatusType statusType = packet.ReadEnum<StatusType>();
                switch (statusType)
                {
                    case StatusType.LifeState:
                        session.Context.LifeState = packet.ReadEnum<LifeState>();
                        break;
                    case StatusType.MotionState:
                        break;
                    case StatusType.GameState:
                        session.Context.GameState = packet.ReadEnum<GameState>();
                        break;
                    case StatusType.PVPState:
                        break;
                    case StatusType.CombatState:
                        break;
                    case StatusType.ScrollingState:
                        break;
                    default:
                        break;
                }
			}
			return PacketResult.None;
		}
	}
}
