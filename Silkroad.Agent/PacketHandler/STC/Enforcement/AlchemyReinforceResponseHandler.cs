﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.DAL;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using System.Threading.Tasks;

namespace Silkroad.Agent.PacketHandler.STC.Enforcement
{
    /// <summary>
    /// S->C SERVER_AGENT_ENFORCEMENT_REINFORCE_RESPONSE = 0xB150
    /// </summary>
    public class AlchemyReinforceResponseHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            if (Settings.PLUS_NOTICE_REQ > 0)
            {
                packet.ReadEnum<ResultCode>();
                packet.ReadEnum<AlchemyAction>();
                var success = packet.ReadValue<bool>(); // Success or Failed
                if (success)
                {
                    byte slot = packet.ReadValue<byte>();
                    var ukn = packet.ReadValue<uint>(); // Ukn
                    uint refItemId = packet.ReadValue<uint>();
                    byte plus = packet.ReadValue<byte>();
                    if (plus >= Settings.PLUS_NOTICE_REQ)
                    {
                        Task.Run(async () => await SendPlusNotice(session, plus, refItemId, slot));
                    }
                }
            }
           
            return PacketResult.None;
        }

        private async Task SendPlusNotice(AgentSession session, int plus, uint refItemId, byte slot)
        {
            Task<string> getNameTask = SqlHelper.GetItemNameByRefItemIDAsync(refItemId);
            Task<int> getAdvTask = SqlHelper.GetAdvancedElixirValueAsync(session.Context.CharName, slot);
            await Task.WhenAll(getNameTask, getAdvTask);
            string itemName = getNameTask.Result;
            int advValue = getAdvTask.Result;
            if (advValue >= 1)
            {
                session.SendGlobalNotice($"[{session.Context.CharName}] has successfully enhanced [{itemName}] to [plus {plus + advValue} Adv.{advValue} Included]");
            }
            else
            {
                session.SendGlobalNotice($"[{session.Context.CharName}] has successfully enhanced [{itemName}] to [plus {plus}]");
            }
        }
    }
}
