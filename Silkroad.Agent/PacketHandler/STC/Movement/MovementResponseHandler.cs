﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.STC.Movement
{
    /// <summary>
    /// S->C SERVER_AGENT_MOVEMENT_RESPONSE = 0xB021
    /// </summary>
    public class MovementResponseHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            if (Settings.AFK_DETECTION)
            {
                session.Context.PingCounter = 0;
                if (session.Context.AutoInverstExp != AutoInverstExp.None &&
                    session.Context.LifeState != LifeState.Dead &&
                    session.Context.PVPMode == PVPMode.None)
                {
                    Packet questionMark = new Packet(Opcode.CLIENT_AGENT_QUESTION_MARK_REQUEST);
                    questionMark.WriteValue<byte>(AutoInverstExp.None);
                    session.SendAsync(PacketType.Server, questionMark);
                    session.Context.AfkStatus = false;
                }
            }
            return PacketResult.None;
        }
    }
}
