﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.STC.QuestMark
{
    /// <summary>
    /// S->C SERVER_AGENT_QUESTION_MARK_RESPONSE = 0xB402
    /// </summary>
    public class QuestionMarkResponseHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            uint uniqueId = packet.ReadValue<uint>();
            if (uniqueId == session.Context.UniqueId)
            {
                session.Context.AutoInverstExp = packet.ReadEnum<AutoInverstExp>();
            }
            return PacketResult.None;
        }
    }
}
