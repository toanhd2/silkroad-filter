﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.DAL;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using System.Threading.Tasks;

namespace Silkroad.Agent.PacketHandler.STC.Chat
{
    /// <summary>
    /// S->C SERVER_AGENT_CHAT_UPDATE_RESPONSE = 0x3026
    /// </summary>
    public class ChatUpdateResponseHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            // Currently, we only logging global chat
            if (!Settings.LOG_GLOBALCHAT) return PacketResult.None;

            ChatType chatType = packet.ReadEnum<ChatType>();
            switch (chatType)
            {
                case ChatType.Global:
                    string sender = packet.ReadValue<string>();
                    if (sender == session.Context.CharName)
                    {
                        string content = packet.ReadValue<string>().ValidateSqlInjection();
                        Task.Run(() => SqlHelper.LogChatAsync(chatType, sender, content));
                    }
                    break;
            }

            return PacketResult.None;
        }
    }
}
