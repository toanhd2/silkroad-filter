﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.STC.Auth
{
    /// <summary>
    /// S->C SERVER_AGENT_AUTH_RESPONSE = 0xA103
    /// </summary>
    public class AuthResponseHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            if (packet.ReadValue<byte>() == 1)
            {
                session.Context.IsLogged = true;
                session.Context.IsGameMaster = Settings.GM_ACCOUNTS.Contains(session.Context.UserName);
                session.Context.CharScreen = true;
            }
            return PacketResult.None;
        }
    }
}
