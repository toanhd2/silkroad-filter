﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.STC.PVP
{
    /// <summary>
    /// S->C SERVER_AGENT_PVP_UPDATE_RESPONSE = 0xB516
    /// </summary>
    public class PVPResponseHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            ResultCode resultCode = packet.ReadEnum<ResultCode>();
            if (resultCode == ResultCode.Success)
            {
                uint uniqueId = packet.ReadValue<uint>();
                PVPMode pvpMode = packet.ReadEnum<PVPMode>();
                if (uniqueId == session.Context.UniqueId)
                {
                    session.Context.PVPMode = pvpMode;
                }
            }
            return PacketResult.None;
        }
    }
}
