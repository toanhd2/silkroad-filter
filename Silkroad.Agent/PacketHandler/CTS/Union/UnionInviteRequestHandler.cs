﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Constants;
using Silkroad.Framework.DAL;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.CTS.Union
{
    /// <summary>
    /// C->S CLIENT_AGENT_UNION_INVITE_REQUEST = 0x70FB
    /// </summary>
    public class UnionInviteRequestHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            if (Settings.UNION_LIMIT > 0)
            {
                var count = SqlHelper.CountUnionMemberByCharName(session.Context.CharName);
                if (count >= Settings.UNION_LIMIT)
                {
                    session.SendNotice(NoticeCode.UNION_LIMIT);
                    return PacketResult.Ignore;
                }
            }

            return PacketResult.None;
        }
    }
}
