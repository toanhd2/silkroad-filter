﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Constants;
using Silkroad.Framework.DAL;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.CTS.Guild
{
    /// <summary>
    /// C->S CLIENT_AGENT_GUILD_INVITE_REQUEST = 0x70F3
    /// </summary>
    public class GuidInviteRequestHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            if (Settings.GUILD_LIMIT > 0)
            {
                var count = SqlHelper.CountGuildMemberByCharName(session.Context.CharName);
                if (count >= Settings.GUILD_LIMIT)
                {
                    session.SendNotice(NoticeCode.GUILD_LIMIT);
                    return PacketResult.Ignore;
                }
            }

            return PacketResult.None;
        }
    }
}
