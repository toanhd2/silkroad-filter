﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.CTS.Mastery
{
    /// <summary>
    /// C->S CLIENT_UP_MASTERY = 0x70A2
    /// </summary>
    public class MasteryUpdateRequestHandler : IPacketHandler<AgentSession>
    {

        public PacketResult Handle(AgentSession session, Packet packet)
        {
            packet.ReadValue<uint>(); // masteryId
            byte level = packet.ReadValue<byte>();
            // Skill Exploit - 0x70A2 - https://www.maxigame.com/forum/t/251583-meshur-vsro-mastery-exploit-ini-delirius-engelleme
            return level == 0x01 ? PacketResult.None : PacketResult.Ignore;
        }
    }
}
