﻿using Microsoft.Extensions.Logging;
using Silkroad.Agent.Network;
using Silkroad.Framework.Constants;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using Silkroad.Utility;

namespace Silkroad.Agent.PacketHandler.CTS.CharSelection
{
    /// <summary>
    /// C->S CLIENT_AGENT_CHARACTER_SELECTION_RENAME_REQUEST = 0x7450
    /// </summary>
    public class CharacterRenameRequestHandler : IPacketHandler<AgentSession>
    {
        private readonly ILogger<CharacterRenameRequestHandler> _log = LoggerManager.GetInstance<CharacterRenameRequestHandler>();

        public PacketResult Handle(AgentSession session, Packet packet)
        {
            if (!session.Context.CharScreen)
            {
                _log.LogWarning(TemplateMessage.Warning.EXPLOIT, session.IpAddress, session.Context.CharName);
                return PacketResult.Ignore;
            }
            return PacketResult.None;
        }
    }
}
