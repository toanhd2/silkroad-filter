﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.CTS.CharSelection
{
    /// <summary>
    /// C->S CLIENT_AGENT_CHARACTER_SCREEN_REQUEST = 0x7007
    /// </summary>
    public class CharacterScreenRequestHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            if (!session.Context.CharScreen)
            {
                return PacketResult.Ignore;
            }

            var action = packet.ReadEnum<CharSelectionAction>();
            switch (action)
            {
                case CharSelectionAction.List:
                    if (packet.Length > 1)
                    {
                        return PacketResult.Disconnect;
                    }
                    break;
                case CharSelectionAction.Delete:
                    {
                        var charNameLength = packet.ReadValue<string>().Length;
                        if ((packet.Length - charNameLength) != 3)
                        {
                            return PacketResult.Disconnect;
                        }
                        break;
                    }
                case CharSelectionAction.CheckName:
                    {
                        var charNameLength = packet.ReadValue<string>().Length;
                        if ((packet.Length - charNameLength) != 3)
                        {
                            return PacketResult.Disconnect;
                        }
                        break;
                    }
                case CharSelectionAction.Restore:
                    {
                        var charNameLength = packet.ReadValue<string>().Length;
                        if ((packet.Length - charNameLength) != 3)
                        {
                            return PacketResult.Disconnect;
                        }
                        break;
                    }
                default:
                    return PacketResult.Disconnect;
            }

            return PacketResult.None;
        }
    }
}
