﻿using Microsoft.Extensions.Logging;
using Silkroad.Agent.Network;
using Silkroad.Framework.Constants;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using Silkroad.Utility;

namespace Silkroad.Agent.PacketHandler.CTS.CharSelection
{
    /// <summary>
    /// C->S CLIENT_AGENT_CHARACTER_SELECTION_REQUEST = 0x7001
    /// </summary>
    public class CharacterSelectionRequestHandler : IPacketHandler<AgentSession>
    {
        private readonly ILogger<CharacterSelectionRequestHandler> _log = LoggerManager.GetInstance<CharacterSelectionRequestHandler>();

        public PacketResult Handle(AgentSession session, Packet packet)
        {
            /*
             Fix IWA exploit
             For more information: https://www.elitepvpers.com/forum/sro-pserver-guides-releases/4232366-release-disconnect-players-exploit-found-iwa-3.html
             */
            if (!session.Context.IsLogged)
            {
                _log.LogWarning(TemplateMessage.Warning.IWA_EXPLOIT, session.IpAddress, session.Context.CharName);
                return PacketResult.Disconnect;
            }

            if (session.Context.IsSelectedChar)
            {
                return PacketResult.Ignore;
            }
            
            // If length of character name invalid
            if (packet.Length <= 2)
            {
                var charName = packet.ReadValue<string>();
                _log.LogWarning(TemplateMessage.Warning.EXPLOIT, session.IpAddress, charName);
                return PacketResult.Disconnect;
            }

            //Register selected character
            session.Context.IsSelectedChar = true;
            session.Context.CharName = packet.ReadValue<string>().ValidateCharacterName();
            return PacketResult.None;
        }
    }
}
