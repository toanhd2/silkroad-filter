﻿using Microsoft.Extensions.Logging;
using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Constants;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using Silkroad.Utility;
using System.Linq;

namespace Silkroad.Agent.PacketHandler.CTS.Events
{
    /// <summary>
    /// C->S BATTLE_ARENA_REGISTER_REQUEST = 0x74D3
    /// </summary>
    public class BattleArenaRegisterRequestHandler : IPacketHandler<AgentSession>
    {
        private readonly ILogger<BattleArenaRegisterRequestHandler> _log = LoggerManager.GetInstance<BattleArenaRegisterRequestHandler>();

        public PacketResult Handle(AgentSession session, Packet packet)
        {
            if (Settings.BA_LEVEL > 0 &&
                session.Context.CurLevel < Settings.BA_LEVEL)
            {
                session.SendNotice(NoticeCode.BA_LEVEL);
                return PacketResult.Ignore;
            }

            var limitResult = HandleBALimit(session);
            if (limitResult.Action != PacketResultAction.None)
            {
                return limitResult;
            }

            return PacketResult.None;
        }

        private PacketResult HandleBALimit(AgentSession session)
        {
            if (Settings.BA_PC_LIMIT <= 0) return PacketResult.None;

            var count = session.Server.Sessions.Values.Count(x => x.HardwareId == session.HardwareId && x.Context.BAState);
            if (count >= Settings.BA_PC_LIMIT)
            {
                _log.LogInformation(TemplateMessage.Infomation.BA_PC_LIMIT, session.Context.UserName, session.HardwareId);
                session.SendNotice(NoticeCode.BA_PC_LIMIT);
                return PacketResult.Ignore;
            }

            return PacketResult.None;
        }
    }
}

