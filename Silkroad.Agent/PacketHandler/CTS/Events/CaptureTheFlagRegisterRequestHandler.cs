﻿using Microsoft.Extensions.Logging;
using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Constants;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using Silkroad.Utility;
using System.Linq;

namespace Silkroad.Agent.PacketHandler.CTS.Events
{
    /// <summary>
    /// C->S CAPTURE_THE_FLAG_REGISTER_REQUEST = 0x74B2
    /// </summary>
    public class CaptureTheFlagRegisterRequestHandler : IPacketHandler<AgentSession>
    {
        private readonly ILogger<CaptureTheFlagRegisterRequestHandler> _log = LoggerManager.GetInstance<CaptureTheFlagRegisterRequestHandler>();

        public PacketResult Handle(AgentSession session, Packet packet)
        {
            if (Settings.CTF_LEVEL > 0 &&
                session.Context.CurLevel < Settings.CTF_LEVEL)
            {
                session.SendNotice(NoticeCode.CTF_LEVEL);
                return PacketResult.Ignore;
            }

            var limitResult = HandleCaptureTheFlagPCLimit(session);
            if (limitResult.Action != PacketResultAction.None)
            {
                return limitResult;
            }

            return PacketResult.None;
        }

        private PacketResult HandleCaptureTheFlagPCLimit(AgentSession session)
        {
            if (Settings.CTF_PC_LIMIT <= 0) return PacketResult.None;

            var count = session.Server.Sessions.Values.Count(x => x.HardwareId == session.HardwareId && x.Context.CTFState);
            if (count >= Settings.CTF_PC_LIMIT)
            {
                _log.LogInformation(TemplateMessage.Infomation.CTF_PC_LIMIT, session.Context.UserName, session.HardwareId);
                session.SendNotice(NoticeCode.CTF_PC_LIMIT);
                return PacketResult.Ignore;
            }

            return PacketResult.None;
        }
    }
}

