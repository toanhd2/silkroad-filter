﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Constants;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using Silkroad.Utility;

namespace Silkroad.Agent.PacketHandler.CTS.FortresWar
{
    /// <summary>
    /// CLIENT_AGENT_SIEGE_ACTION = 0x705E
    /// </summary>
    public class SiegeActionRequestHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            packet.ReadValue<uint>();
            var actionType = packet.ReadEnum<SiegeActionType>();
            switch (actionType)
            {
                case SiegeActionType.TaxRateChange when Settings.DISABLE_TAX_RATE_CHANGE:
                    {
                        session.SendNotice(NoticeCode.DISABLE_TAX_RATE_CHANGE);
                        return PacketResult.Ignore;
                    }

                case SiegeActionType.RegistrationFortressWar:
                    {
                        var fortressType = packet.ReadEnum<FortressType>();
                        switch (fortressType)
                        {
                            case FortressType.Jangan when Settings.DISABLE_JG_FW_REG:
                                session.SendNotice(NoticeCode.DISABLE_JG_FW_REG);
                                return PacketResult.Ignore;

                            case FortressType.Bandit when Settings.DISABLE_BA_FW_REG:
                                session.SendNotice(NoticeCode.DISABLE_BA_FW_REG);
                                return PacketResult.Ignore;

                            case FortressType.Hotan when Settings.DISABLE_HT_FW_REG:
                                session.SendNotice(NoticeCode.DISABLE_HT_FW_REG);
                                return PacketResult.Ignore;
                        }
                        break;
                    }

                case SiegeActionType.AboutGuildChange:
                    {
                        packet.ReadValue<uint>();
                        packet.ReadValue<ushort>();
                        string message = packet.ReadValue<string>();
                        // SQL Injection - 0x705E - 
                        // https://www.elitepvpers.com/forum/sro-private-server/4141360-information-sql-injection-ingame.html
                        if (Validation.HasSpecialCharacter(message))
                        {
                            session.SendNotice(NoticeCode.NOT_ALLOWED_SPECIAL_CHARACTERS);
                            return PacketResult.Ignore;
                        }
                        break;
                    }
            }
            return PacketResult.None;
        }
    }
}
