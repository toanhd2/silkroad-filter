﻿using Microsoft.Extensions.Logging;
using Silkroad.Agent.Network;
using Silkroad.Framework.Constants;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using Silkroad.Utility;

namespace Silkroad.Agent.PacketHandler.CTS.Enforcement
{
    /// <summary>
    /// CLIENT_AGENT_ENFORCEMENT_MAGIC_OPTION_GRANT_REQUEST = 0x34A9
    /// </summary>
    public class MagicOptionGrantRequestHandler : IPacketHandler<AgentSession>
    {
        private readonly ILogger<MagicOptionGrantRequestHandler> _log = LoggerManager.GetInstance<MagicOptionGrantRequestHandler>();
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            var avatarBlue = packet.ReadValue<string>().ToLower();
            // Avatar Exploit - 0x34A9 - https://www.elitepvpers.com/forum/sro-pserver-guides-releases/3991992-release-invincible-avatar-magopt-exploit.html
            if (!avatarBlue.Contains("avatar"))
            {
                _log.LogWarning(TemplateMessage.Warning.AVATAR_MAGIC_EXPLOIT, session.IpAddress, session.Context.CharName);
                return PacketResult.Disconnect;
            }

            return PacketResult.None;
        }
    }
}
