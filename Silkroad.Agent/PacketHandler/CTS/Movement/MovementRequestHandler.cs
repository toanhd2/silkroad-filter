﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.CTS.Movement
{
    /// <summary>
    /// C->S SERVER_AGENT_MOVEMENT_REQUEST = 0x7021
    /// </summary>
    public class MovementRequestHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            
            return PacketResult.None;
        }

    }
}
