﻿using Microsoft.Extensions.Logging;
using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Constants;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using Silkroad.Utility;
using System.Linq;

namespace Silkroad.Agent.PacketHandler.CTS.Auth
{
    /// <summary>
    /// C->S CLIENT_AGENT_AUTH_REQUEST = 0x6103
    /// </summary>
    public class AuthRequestHandler : IPacketHandler<AgentSession>
    {
        private readonly ILogger<AuthRequestHandler> _log = LoggerManager.GetInstance<AuthRequestHandler>();
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            session.Token = packet.ReadValue<uint>();
            session.Context.UserName = packet.ReadValue<string>().ValidateSqlInjection();
            session.HardwareId = Global.GetHardwareIdByToken(session.Token);

            var pcLimitResult = HandlePCLimit(session);
            if (pcLimitResult.Action != PacketResultAction.None)
            {
                return pcLimitResult;
            }

            var ipLimitResult = HandleIPLimit(session);
            if (ipLimitResult.Action != PacketResultAction.None)
            {
                return ipLimitResult;
            }

            return PacketResult.None;
        }

        private PacketResult HandlePCLimit(AgentSession session)
        {
            if (Settings.PC_LIMIT <= 0) return PacketResult.None;

            var count = session.Server.Sessions.Values.Count(x => x.HardwareId == session.HardwareId);
            if (count >= Settings.PC_LIMIT)
            {
                _log.LogInformation(TemplateMessage.Infomation.PC_LIMIT, session.Context.UserName, session.HardwareId);
                Packet packet = new Packet(0xA102);
                packet.WriteValue<byte>(0x02);
                packet.WriteValue<byte>(12); // PC LIMIT ERROR
                session.Send(packet);
                return PacketResult.Disconnect;
            }

            return PacketResult.None;
        }

        private PacketResult HandleIPLimit(AgentSession session)
        {
            if (Settings.IP_LIMIT <= 0) return PacketResult.None;

            var currentPlayersPerIp = session.Server.Sessions.Values.Count(x => x.IpAddress == session.IpAddress);
            if (Settings.CAFE_IP_LIMIT > 0 && (Settings.NETCAFE_IPS.Contains(session.IpAddress)))
            {
                if (currentPlayersPerIp >= Settings.CAFE_IP_LIMIT)
                {
                    _log.LogInformation(TemplateMessage.Infomation.IP_LIMIT, session.Context.UserName, session.IpAddress);
                    Packet packet = new Packet(0xA102);
                    packet.WriteValue<byte>(0x02);
                    packet.WriteValue<byte>(8); // IC LIMIT ERROR
                    session.Send(packet);
                    return PacketResult.Disconnect;
                }
            }
            else
            {
                if (currentPlayersPerIp >= Settings.IP_LIMIT)
                {
                    _log.LogInformation(TemplateMessage.Infomation.IP_LIMIT, session.Context.UserName, session.IpAddress);
                    Packet packet = new Packet(0xA102);
                    packet.WriteValue<byte>(0x02);
                    packet.WriteValue<byte>(8); // IC LIMIT ERROR
                    session.Send(packet);
                    return PacketResult.Disconnect;
                }
            }

            return PacketResult.None;
        }
    }
}
