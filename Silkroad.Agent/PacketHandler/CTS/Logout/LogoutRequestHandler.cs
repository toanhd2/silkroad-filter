﻿using Microsoft.Extensions.Logging;
using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Constants;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using Silkroad.Utility;
using System;

namespace Silkroad.Agent.PacketHandler.CTS.Logout
{
    /// <summary>
    /// C->S CLIENT_AGENT_LOGOUT_REQUEST = 0x7005
    /// </summary>
    public class LogoutRequestHandler : IPacketHandler<AgentSession>
    {
        private readonly ILogger<LogoutRequestHandler> _log = LoggerManager.GetInstance<LogoutRequestHandler>();
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            /*
             Fix IWA exploit
             For more information: https://www.elitepvpers.com/forum/sro-pserver-guides-releases/3991992-release-invincible-avatar-magopt-exploit.html
             */
            if (packet.Length != 1)
            {
                _log.LogWarning(TemplateMessage.Warning.EXPLOIT, session.IpAddress, session.Context.CharName);
                return PacketResult.Disconnect;
            }

            var actionType = packet.ReadEnum<ExitActionType>();
            switch (actionType)
            {
                case ExitActionType.Exit:
                    {
                        if (Settings.LOGOUT_DELAY > 0)
                        {
                            var remainingTime = DateTime.Now.Subtract(session.Context.LastLogOutTime).TotalSeconds;
                            if (remainingTime < Settings.LOGOUT_DELAY)
                            {
                                session.SendNotice(NoticeCode.EXIT_DELAY);
                                return PacketResult.Ignore;
                            }
                        }
                        session.Context.LastLogOutTime = DateTime.Now;
                        break;
                    }

                case ExitActionType.Restart:
                    {
                        if (Settings.RESTART_DISABLE)
                        {
                            session.SendNotice(NoticeCode.RESTART_DISABLE);
                            return PacketResult.Ignore;
                        }

                        if (Settings.RESTART_DELAY > 0)
                        {
                            var remainingTime = DateTime.Now.Subtract(session.Context.LastRestartTime).TotalSeconds;
                            if (remainingTime < Settings.RESTART_DELAY)
                            {
                                session.SendNotice(NoticeCode.RESTART_DELAY);
                                return PacketResult.Ignore;
                            }
                        }
                        session.Context.LastRestartTime = DateTime.Now;
                        break;
                    }

                default:
                    return PacketResult.Disconnect;
            }

            return PacketResult.None;
        }
    }
}
