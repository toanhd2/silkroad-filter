﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Constants;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.CTS.Academy
{
    /// <summary>
    /// C->S CLIENT_AGENT_ACADEMY_CREATION_REQUEST = 0x7470
    /// </summary>
    public class AcademyCreationRequestHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            if (Settings.DISABLED_ACADEMY)
            {
                session.SendNotice(NoticeCode.ACADEMY_CREATION);
                return PacketResult.Ignore;
            }
            return PacketResult.None;
        }
    }
}
