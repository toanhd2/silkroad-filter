﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Constants;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.CTS.Academy
{
    /// <summary>
    /// C->S CLIENT_AGENT_ACADEMY_INVITE_REQUEST = 0x7472
    /// </summary>
    public class AcademyInviteRequestHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            if (Settings.DISABLED_ACADEMY)
            {
                session.SendNotice(NoticeCode.ACADEMY_INVITE);
                return PacketResult.Ignore;
            }
            return PacketResult.None;
        }
    }
}
