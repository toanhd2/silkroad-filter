﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.CTS.Exchange
{
    /// <summary>
    /// C->S CLIENT_AGENT_EXCHANGE_ACCEPT_REQUEST = 0x7082
    /// </summary>
    public class ExchangeAcceptRequestHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            session.Context.BlockStall = true;
            return PacketResult.None;
        }
    }
}
