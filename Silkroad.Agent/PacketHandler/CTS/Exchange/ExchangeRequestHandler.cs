﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Constants;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using System;

namespace Silkroad.Agent.PacketHandler.CTS.Exchange
{
    /// <summary>
    /// C->S CLIENT_AGENT_EXCHANGE_START_REQUEST = 0x7081
    /// </summary>
    public class ExchangeRequestHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            session.Context.BlockStall = true;

            if (Settings.EXCHANGE_DELAY > 0)
            {
                var remainingTime = DateTime.Now.Subtract(session.Context.LastSentExchangeTime).TotalSeconds;
                if (remainingTime < Settings.EXCHANGE_DELAY)
                {
                    session.SendNotice(NoticeCode.EXCHANGE_DELAY);
                    return PacketResult.Ignore;
                }
            }

            session.Context.LastSentExchangeTime = DateTime.Now;
            return PacketResult.None;
        }
    }
}
