﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Constants;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.CTS.Job
{
    /// <summary>
    /// C->S CLIENT_JOIN_JOB_REQUEST = 0x70E1
    /// </summary>
    public class JoinJobRequestHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            if (Settings.JOB_LEVEL > 0 && 
                session.Context.CurLevel < Settings.JOB_LEVEL)
            {
                session.SendNotice(NoticeCode.JOB_LEVEL);
                return PacketResult.Ignore;
            }

            return PacketResult.None;
        }
    }
}

