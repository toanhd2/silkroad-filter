﻿using Microsoft.Extensions.Logging;
using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Constants;
using Silkroad.Framework.DAL;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using Silkroad.Utility;
using System;

namespace Silkroad.Agent.PacketHandler.CTS.CharAction
{
    /// <summary>
    /// C->S CLIENT_AGENT_PLAYER_BERSERK = 0x70A7
    /// </summary>
    public class PlayerBerserkRequestHandler : IPacketHandler<AgentSession>
    {
        private readonly ILogger<PlayerBerserkRequestHandler> _log = LoggerManager.GetInstance<PlayerBerserkRequestHandler>();

        public PacketResult Handle(AgentSession session, Packet packet)
        {
            /* 
             Zerk Exploit - 0x70A7
             More infomation: https://www.elitepvpers.com/forum/sro-pserver-guides-releases/3991992-release-invincible-avatar-magopt-exploit.html
             */
            if (packet.ReadValue<byte>() != 1)
            {
                _log.LogWarning(TemplateMessage.Warning.ZERK_EXPLOIT, session.IpAddress, session.Context.CharName);
                return PacketResult.Disconnect;
            }

            if (Settings.ZERK_DELAY > 0)
            {
                var remainingTime = DateTime.Now.Subtract(session.Context.LastZerkTime).TotalSeconds;
                if (remainingTime < Settings.ZERK_DELAY)
                {
                    session.SendNotice(NoticeCode.ZERK_DELAY);
                    return PacketResult.Ignore;
                }
            }

            if (Settings.DISABLE_FELLOW_UNDER_ZERK && session.Context.IsRidingPet)
            {
                bool isFellowPetSummoned = SqlHelper.IsFellowPetSummonedByCharName(session.Context.CharName);
                if (isFellowPetSummoned)
                {
                    session.SendNotice(NoticeCode.DISABLE_ZERK_FELLOW);
                    return PacketResult.Ignore;
                }
            }
            session.Context.LastZerkTime = DateTime.Now;
            return PacketResult.None;
        }
    }
}
