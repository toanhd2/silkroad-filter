﻿using Microsoft.Extensions.Logging;
using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Constants;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using Silkroad.Utility;
using System.Linq;

namespace Silkroad.Agent.PacketHandler.CTS.CharAction
{
    /// <summary>
    /// C->S CLIENT_SPAWN_SUCCESS = 0x3012
    /// </summary>
    public class SpawnSuccessRequestHandler : IPacketHandler<AgentSession>
    {
        private readonly ILogger<SpawnSuccessRequestHandler> _log = LoggerManager.GetInstance<SpawnSuccessRequestHandler>();

        public PacketResult Handle(AgentSession session, Packet packet)
        {
            if (packet.Length > 0)
            {
                _log.LogWarning(TemplateMessage.Warning.EXPLOIT, session.IpAddress, session.Context.CharName);
                return PacketResult.Disconnect;
            }

            session.Server.RegisterSession(session);
            if (session.Context.IsFirstSpawn)
            {
                var pcLimitResult = HandlePCLimit(session);
                if (pcLimitResult.Action != PacketResultAction.None)
                {
                    return pcLimitResult;
                }

                var ipLimitResult = HandleIPLimit(session);
                if (ipLimitResult.Action != PacketResultAction.None)
                {
                    return ipLimitResult;
                }

                HandleWelcomeMessage(session);
            }

            session.Context.IsFirstSpawn = false;
            return PacketResult.None;
        }

        private void HandleWelcomeMessage(AgentSession session)
        {
            if (Settings.WELCOME_MSG)
            {
                session.SendNotice(NoticeCode.WELCOME_MSG);
            }
        }

        private PacketResult HandlePCLimit(AgentSession session)
        {
            if (Settings.PC_LIMIT <= 0) return PacketResult.None;

            var count = session.Server.Sessions.Values.Count(x => x.HardwareId == session.HardwareId);
            if (count > Settings.PC_LIMIT)
            {
                _log.LogInformation(TemplateMessage.Infomation.PC_LIMIT, session.Context.UserName, session.HardwareId);
                session.SendNotice(NoticeCode.PC_LIMIT, false);
                return PacketResult.Disconnect;
            }

            return PacketResult.None;
        }

        private PacketResult HandleIPLimit(AgentSession session)
        {
            if (Settings.IP_LIMIT <= 0) return PacketResult.None;

            var currentPlayersPerIp = session.Server.Sessions.Values.Count(x => x.IpAddress == session.IpAddress);
            if (Settings.CAFE_IP_LIMIT > 0 && (Settings.NETCAFE_IPS.Contains(session.IpAddress)))
            {
                if (currentPlayersPerIp > Settings.CAFE_IP_LIMIT)
                {
                    _log.LogInformation(TemplateMessage.Infomation.IP_LIMIT, session.Context.UserName, session.IpAddress);
                    session.SendNotice(NoticeCode.IP_LIMIT, false);
                    return PacketResult.Disconnect;
                }
            }
            else
            {
                if (currentPlayersPerIp > Settings.IP_LIMIT)
                {
                    _log.LogInformation(TemplateMessage.Infomation.IP_LIMIT, session.Context.UserName, session.IpAddress);
                    session.SendNotice(NoticeCode.IP_LIMIT, false);
                    return PacketResult.Disconnect;
                }
            }
            
            return PacketResult.None;
        }
    }
}
