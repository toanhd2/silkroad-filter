﻿using Microsoft.Extensions.Logging;
using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Constants;
using Silkroad.Framework.DAL;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using Silkroad.Utility;
using System.Linq;

namespace Silkroad.Agent.PacketHandler.CTS.CharAction
{
    /// <summary>
    /// C->S CLIENT_ITEM_MOVE_REQUEST = 0x7034
    /// </summary>
    public class ItemMoveRequestHandler : IPacketHandler<AgentSession>
    {
        private readonly ILogger<ItemMoveRequestHandler> _log = LoggerManager.GetInstance<ItemMoveRequestHandler>();

        public PacketResult Handle(AgentSession session, Packet packet)
        {
            var actionType = packet.ReadEnum<ItemActionType>();
            switch (actionType)
            {
                case ItemActionType.Equip:
                    packet.ReadValue<byte>(); // from slot
                    var charSlot = packet.ReadValue<byte>();
                    if (charSlot == 8)
                    {
                        #region DISABLE FELLOW UNDER JOB
                        var isSummonedFellowPet = SqlHelper.IsFellowPetSummonedByCharName(session.Context.CharName);
                        if (isSummonedFellowPet)
                        {
                            session.SendNotice(NoticeCode.DISABLE_FELLOW_JOB);
                            return PacketResult.Ignore;
                        }
                        #endregion

                        #region JOB PC LIMIT
                        var jobPCLimitResult = HandleJobPCLimit(session);
                        if (jobPCLimitResult.Action != PacketResultAction.None)
                        {
                            return jobPCLimitResult;
                        }
                        #endregion
                    }
                    break;

                case ItemActionType.Drop:
                    #region DISABLE GOLDITEM DROP INTOWN
                    if (Settings.TOWN_DROP_ITEM)
                    {
                        var latestRegion = SqlHelper.GetLatestRegionByCharName(session.Context.CharName);
                        if (Settings.TOWN_REGIONS.Contains(latestRegion))
                        {
                            session.SendNotice(NoticeCode.DISABLE_GOLDITEM_DROP_INTOWN);
                            return PacketResult.Ignore;
                        }
                    }
                    #endregion
                    break;

                case ItemActionType.MoveToPet:
                    #region FIX CRASH SR_GAMESERVER
                    packet.ReadValue<uint>();
                    packet.ReadValue<byte>();
                    byte to_slot = packet.ReadValue<byte>();
                    if (to_slot >= 56)
                    {
                        _log.LogWarning(TemplateMessage.Warning.PET_EXPLOIT);
                        return PacketResult.Ignore;
                    }
                    #endregion
                    break;
            }

            #region ITEM LOCK
            //TODO: Implemnt
            #endregion
            return PacketResult.None;
        }

        private PacketResult HandleJobPCLimit(AgentSession session)
        {
            if (Settings.PC_LIMIT <= 0) return PacketResult.None;

            var count = session.Server.Sessions.Values.Count(x => x.HardwareId == session.HardwareId && x.Context.JobState);
            if (count >= Settings.JOB_PC_LIMIT)
            {
                _log.LogInformation(TemplateMessage.Infomation.JOB_PC_LIMIT, session.Context.UserName, session.HardwareId);
                session.SendNotice(NoticeCode.JOB_PC_LIMIT);
                return PacketResult.Ignore;
            }

            return PacketResult.None;
        }
    }
}
