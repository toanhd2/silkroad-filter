﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Constants;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.CTS.CharAction
{
    /// <summary>
    /// C->S CLIENT_AGENT_OBJECT_ACTION_REQUEST = 0x7074
    /// </summary>
    public class ObjectActionRequestHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            var actionFlag = packet.ReadEnum<ObjectActionFlag>();
            if (actionFlag == ObjectActionFlag.Cancel)
                return PacketResult.None;

            var actionType = packet.ReadEnum<ObjectActionType>();
            switch (actionType)
            {
                case ObjectActionType.Trace:
                    if (Settings.ANTI_JOB_TRACE && session.Context.JobState)
                    {
                        session.SendNotice(NoticeCode.JOB_TRACE);
                        return PacketResult.Ignore;
                    }

                    if (Settings.FTW_TRACE && session.Context.AtFortressWar)
                    {
                        session.SendNotice(NoticeCode.FTW_TRACE);
                        return PacketResult.Ignore;
                    }
                    break;

                case ObjectActionType.UseSkill:
                    var skillId = packet.ReadValue<ushort>();
                    if (Settings.ANTI_SKILL &&
                        Settings.BLOCKED_SKILLS.Contains(skillId))
                    {
                        session.SendNotice(NoticeCode.BLOCK_SKILL);
                        return PacketResult.Ignore;
                    }

                    if (Settings.ANTI_JOB_SKILL &&
                        session.Context.JobState && Settings.BLOCKED_JOB_SKILLS.Contains(skillId))
                    {
                        session.SendNotice(NoticeCode.BLOCK_JOB_SKILL);
                        return PacketResult.Ignore;
                    }

                    if (Settings.ANTI_FORTRESS_SKILL && 
                        session.Context.AtFortressWar && Settings.BLOCKED_FTW_SKILLS.Contains(skillId))
                    {
                        session.SendNotice(NoticeCode.BLOCK_FTW_SKILL);
                        return PacketResult.Ignore;
                    }
                    break;
            }

            return PacketResult.None;
        }
    }
}
