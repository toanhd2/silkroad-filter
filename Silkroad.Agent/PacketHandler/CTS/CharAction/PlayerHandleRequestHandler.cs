﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Constants;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Extensions;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using System;
using System.Linq;

namespace Silkroad.Agent.PacketHandler.CTS.CharAction
{
    /// <summary>
    /// C->S CLIENT_AGENT_PLAYER_HANDLE_REQUEST = 0x704C
    /// </summary>
    public class PlayerHandleRequestHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            packet.ReadValue<byte>(); // InventorySlot
            byte type = packet.ReadValue<byte>(); //Type(EC: Normal | ED: ItemMall)
            byte typeId = packet.ReadValue<byte>(); //TypeID (08: HP | 09: ? | 10: MP)
            return typeId switch
            {
                // FELLOW PET
                0x08 when type is 0xCD => HandleFellowPet(session),
                // GLOBAL SCROLL
                0x19 when type is 0xEC || type is 0xED => HandleReverseScroll(session, packet),
                // GLOBAL SCROLL
                0x29 when type is 0xEC || type is 0xED => HandleGlobalScroll(session, packet),
                // RESCURRENT SCROLL
                0x36 when type is 0xEC || type is 0xED => HandleResCurrentScroll(session),
                _ => PacketResult.None
            };
        }

        private PacketResult HandleFellowPet(AgentSession session)
        {
            if (session.Context.JobState && Settings.DISABLE_FELLOW_UNDER_JOB)
            {
                session.SendNotice(NoticeCode.DISABLE_FELLOW_JOB);
                return  PacketResult.Ignore;
            }
            return PacketResult.None;
        }

        private PacketResult HandleGlobalScroll(AgentSession session, Packet packet)
        {
            #region GLOBAL REQUIRE LEVEL
            if (Settings.GLOBAL_LEVEL > 0 &&
                session.Context.CurLevel < Settings.GLOBAL_LEVEL)
            {
                session.SendNotice(NoticeCode.GLOBAL_LEVEL);
                return PacketResult.Ignore;
            }
            #endregion

            #region CHAT FILTER
            string message = packet.ReadValue<string>();
            if (Settings.BadWords.Any(message.Contains))
            {
                session.SendNotice(NoticeCode.CHAT_FILTER);
                return PacketResult.Ignore;
            }
            #endregion

            #region GLOBAL DELAY
            if (Settings.GLOBAL_DELAY > 0)
            {
                var remainingTime = DateTime.Now.Subtract(session.Context.LastGlobalTime).TotalSeconds;
                if (remainingTime < Settings.GLOBAL_DELAY)
                {
                    session.SendNotice(NoticeCode.GLOBAL_DELAY);
                    return PacketResult.Ignore;
                }
                session.Context.LastGlobalTime = DateTime.Now;
            }
            #endregion

            return PacketResult.None;
        }

        private PacketResult HandleResCurrentScroll(AgentSession session)
        {
            #region BLOCK RES IN JOB
            if (Settings.JOB_RES_SCROLL && session.Context.JobState)
            {
                session.SendNotice(NoticeCode.JOB_RES_SCROLL);
                return PacketResult.Ignore;
            }
            #endregion

            #region BLOCK RES IN FTW
            if (Settings.FTW_RES_SCROLL && session.Context.OnFortressWar)
            {
                session.SendNotice(NoticeCode.FTW_RES_SCROLL);
                return PacketResult.Ignore;
            }
            #endregion

            #region RESCURRENT DELAY
            if (Settings.RESCURRENT_DELAY > 0)
            {
                var remainingTime = DateTime.Now.Subtract(session.Context.LastResCurrentTime).TotalSeconds;
                if (remainingTime < Settings.RESCURRENT_DELAY)
                {
                    session.SendNotice(NoticeCode.RES_SCROLL_DELAY);
                    return PacketResult.Ignore;
                }
                session.Context.LastResCurrentTime = DateTime.Now;
            }
            #endregion

            return PacketResult.None;
        }

        private PacketResult HandleReverseScroll(AgentSession session, Packet packet)
        {
            #region BLOCK REVERSE IN JOB
            if (session.Context.JobState)
            {
                var reverseType = packet.ReadEnum<ReverseType>();
                switch (reverseType)
                {
                    case ReverseType.LastRecallPoint:
                        if (Settings.JOB_REVERSE_LAST_RECALL_POINT)
                        {
                            session.SendNotice(NoticeCode.JOB_REVERSE_LAST_RECALL_POINT);
                            return PacketResult.Ignore;
                        }
                        break;

                    case ReverseType.DeathPoint:
                        if (Settings.JOB_REVERSE_DEATH_POINT)
                        {
                            session.SendNotice(NoticeCode.JOB_REVERSE_DEATH_POINT);
                            return PacketResult.Ignore;
                        }
                        break;

                    case ReverseType.MapPoint:
                        if (Settings.JOB_REVERSE_MAP_POINT)
                        {
                            session.SendNotice(NoticeCode.JOB_REVERSE_MAP_POINT);
                            return PacketResult.Ignore;
                        }
                        break;
                }
            }
            #endregion

            #region REVERSE DELAY
            if (Settings.REVERSE_DELAY > 0)
            {
                var remainingTime = DateTime.Now.Subtract(session.Context.LastReverseTime).TotalSeconds;
                if (remainingTime < Settings.REVERSE_DELAY)
                {
                    session.SendNotice(NoticeCode.REVERSE_SCROLL_DELAY);
                    return PacketResult.Ignore;
                }
                session.Context.LastReverseTime = DateTime.Now;
            }
            #endregion

            return PacketResult.None;
        }
    }
}
