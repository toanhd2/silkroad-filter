﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Constants;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;
using System;
using System.Linq;

namespace Silkroad.Agent.PacketHandler.CTS.Stall
{
    /// <summary>
    /// C->S CLIENT_AGENT_STALL_CREATE_REQUEST = 0x70B1
    /// </summary>
    public class StallCreateRequestHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {

            #region STALL EXPOLIT
            if (session.Context.BlockStall)
            {
                session.SendNotice(NoticeCode.STALL_DELAY);
                return PacketResult.Ignore;
            }
            #endregion

            #region STALL FILTER
            string stallName = packet.ReadValue<string>();
            if (Settings.BadWords.Any(stallName.Contains))
            {
                session.SendNotice(NoticeCode.CHAT_FILTER);
                return PacketResult.Ignore;
            }
            #endregion

            #region STALL DELAY
            if (Settings.STALL_DELAY > 0)
            {
                var remainingTime = DateTime.Now.Subtract(session.Context.LastStallTime).TotalSeconds;
                if (remainingTime < Settings.STALL_DELAY)
                {
                    session.SendNotice(NoticeCode.STALL_DELAY);
                    return PacketResult.Ignore;
                }
            }
            #endregion

            session.Context.LastStallTime = DateTime.Now;
            return PacketResult.None;
        }

    }
}
