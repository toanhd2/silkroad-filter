﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.CTS
{
    /// <summary>
    /// C->S CLIENT_PING_REQUEST = 0x2002
    /// </summary>
    public class PingRequestHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            if (packet.Length > 0)
            {
                return PacketResult.Ignore;
            }

            if (Settings.AFK_DETECTION)
            {
                if (session.Context.PingCounter < 4)
                {
                    session.Context.PingCounter++;
                }
                else
                {
                    session.Context.PingCounter = 0;

                    if (session.Context.AutoInverstExp == AutoInverstExp.None &&
                        session.Context.LifeState != LifeState.Dead &&
                        session.Context.PVPMode == PVPMode.None)
                    {
                        Packet questionMark = new Packet(Opcode.CLIENT_AGENT_QUESTION_MARK_REQUEST);
                        questionMark.WriteValue<byte>(AutoInverstExp.Helpful);
                        session.RemoteSecurity.Send(questionMark);
                        session.Context.AfkStatus = true;
                    }
                }
            }
            
            return PacketResult.None;
        }
    }
}
