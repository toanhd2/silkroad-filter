﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.CTS.QuestionMark
{
    /// <summary>
    /// C->S CLIENT_AGENT_QUESTION_MARK_REQUEST = 0x7402
    /// </summary>
    public class QuestionMarkRequestHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            if (Settings.AFK_DETECTION)
            {
                return PacketResult.Ignore;
            }
            return PacketResult.None;
        }

    }
}
