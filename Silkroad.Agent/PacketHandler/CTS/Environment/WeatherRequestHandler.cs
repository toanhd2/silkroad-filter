﻿using Silkroad.Agent.Network;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Interfaces;
using Silkroad.Framework.Security;

namespace Silkroad.Agent.PacketHandler.CTS.Environment
{
    /// <summary>
    /// C->S CLIENT_WEATHER_REQUEST = 0x750E
    /// </summary>
    public class WeatherRequestHandler : IPacketHandler<AgentSession>
    {
        public PacketResult Handle(AgentSession session, Packet packet)
        {
            if (packet.Length > 0)
            {
                return PacketResult.Disconnect;
            }

            if (Settings.AFK_DETECTION)
            {
                if (session.Context.AutoInverstExp == AutoInverstExp.Helpful)
                {
                    Packet helperMark = new Packet(Opcode.CLIENT_AGENT_QUESTION_MARK_REQUEST);
                    helperMark.WriteValue<byte>(AutoInverstExp.None);
                    session.RemoteSecurity.Send(helperMark);
                    session.Context.AfkStatus = false;
                }
                session.Context.PingCounter = 0;
            }

            return PacketResult.None;
        }
    }
}
