﻿using Microsoft.Extensions.Logging;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Network;
using Silkroad.Framework.Security;
using Silkroad.Utility;
using System;

namespace Silkroad.Agent.Network
{
    public class AgentSession : Session
    {
        private readonly ILogger<AgentSession> _log = LoggerManager.GetInstance<AgentSession>();
        public new AgentServer Server { get; set; }
        public AgentContext Context { get; set; } = new AgentContext();
        public AgentSession(AgentServer agentServer) : base(agentServer)
        {
            Server = agentServer;
        }

        public override void OnReceivedFromClient(byte[] buffer, int offset, int size)
        {
            try
            {
                ClientSecurity.Recv(buffer, 0, size);
                var packets = ClientSecurity.TransferIncoming();
                if (packets == null) return;
                foreach (var packet in packets)
                {
                    //_log.LogError($"Receive packet from client opcode: {packet.Opcode}");

                    if (packet.Opcode == 0x5000 || packet.Opcode == 0x9000 ||
                        packet.Opcode == 0x2001 || packet.Opcode == 0x2113)
                        continue;

                    if (packet.Opcode == 0x7025)
                    {
                        packet.ReadValue<byte>();
                        packet.ReadValue<ushort>();
                        Console.WriteLine(packet.ReadValue<string>());
                    }
                    var result = Server.PacketManager.Handle(PacketType.Client, this, packet);
                    switch (result.Action)
                    {
                        case PacketResultAction.Ignore:
                            continue;

                        case PacketResultAction.Disconnect:
                            Disconnect();
                            return;

                        case PacketResultAction.Replace:
                            foreach (var replacedPacket in result)
                                RemoteSecurity.Send(replacedPacket);
                            continue;

                        case PacketResultAction.Response:
                            foreach (var replacedPacket in result)
                                ClientSecurity.Send(replacedPacket);
                            TransferToClientAsync();
                            continue;
                    }

                    RemoteSecurity.Send(packet);
                }
                TransferToServerAsync();
            }
            catch (Exception ex)
            {
                _log.LogError(ex, ex.Message);
            }
        }

        public override void OnReceivedFromServer(byte[] buffer, int offset, int size)
        {
            try
            {
                //Log.LogError($"Receive packet from server Hex: {buffer.HexDump(offset, size)}");
                RemoteSecurity.Recv(buffer, 0, size);
                var packets = RemoteSecurity.TransferIncoming();
                if (packets == null) return;
                foreach (var packet in packets)
                {
                    //_log.LogInformation($"Receive packet from server Opcode: {packet.Opcode:X4}");

                    if (packet.Opcode == 0x5000 || packet.Opcode == 0x9000)
                        continue;

                    var result = Server.PacketManager.Handle(PacketType.Server, this, packet);
                    switch (result.Action)
                    {
                        case PacketResultAction.Ignore:
                            continue;

                        case PacketResultAction.Disconnect:
                            Disconnect();
                            return;

                        case PacketResultAction.Replace:
                            foreach (var replacedPacket in result)
                                ClientSecurity.Send(replacedPacket);
                            continue;

                        case PacketResultAction.Response:
                            foreach (var replacedPacket in result)
                                RemoteSecurity.Send(replacedPacket);
                            TransferToServerAsync();
                            continue;
                    }

                    ClientSecurity.Send(packet);
                }
                TransferToClientAsync();
            }
            catch (Exception ex)
            {
                _log.LogError(ex, ex.Message);
            }
        }

        /// <summary>
        /// Send notice to client
        /// </summary>
        /// <param name="notice">message content</param>
        /// <param name="useAsync">Use async method to transfer notice to client if true else use sync</param>
        public void SendNotice(string notice, bool useAsync = true)
        {
            Packet packet = new Packet(0x300C);
            packet.WriteValue<ushort>(3100);
            packet.WriteValue<byte>(1);
            packet.WriteValue<string>(notice);
            ClientSecurity.Send(packet);

            packet = new Packet(0x300C);
            packet.WriteValue<ushort>(3100);
            packet.WriteValue<byte>(2);
            packet.WriteValue<string>(notice);
            ClientSecurity.Send(packet);

            if (useAsync)
            {
                TransferToClientAsync();
            }
            else
            {
                TransferToClient();
            }
        }

        public void SendGlobalNotice(string notice)
        {
            foreach (AgentSession session in Server.Sessions.Values)
            {
                Packet packet = new Packet(0x3026);
                packet.WriteValue<byte>((byte)7);
                packet.WriteValue<string>(notice);
                session.ClientSecurity.Send(packet);
                session.TransferToClientAsync();
            }
        }

        public void SendAsync(PacketType packetType, Packet packet)
        {
            switch (packetType)
            {
                case PacketType.Server:
                    RemoteSecurity.Send(packet);
                    TransferToServerAsync();
                    break;
                case PacketType.Client:
                    ClientSecurity.Send(packet);
                    TransferToClientAsync();
                    break;
                default:
                    throw new Exception("Send packet failed because PacketType not support!");
            }
        }
    }
}
