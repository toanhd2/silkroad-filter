﻿using Silkroad.Framework.Enums;
using Silkroad.Framework.Models;
using System;

namespace Silkroad.Agent.Network
{
    public class AgentContext
    {
        public string UserName { get; set; }
        public string CharName { get; set; }
        public bool IsLogged { get; set; } = false;
        public bool IsGameMaster { get; set; }
        public bool CharScreen { get; set; } = false;
        public bool IsSelectedChar { get; set; } = false;
        public bool IsFirstSpawn { get; set; } = true;
        public bool JobState { get; set; }
        public bool OnFortressWar { get; set; } = false;
        public bool IsSentCharName { get; set; } = false;
        public uint UniqueId { get; set; }
        public bool IsRidingPet { get; set; }
        public DateTime LastLogOutTime { get; set; }
        public DateTime LastGlobalTime { get; set; }
        public DateTime LastResCurrentTime { get; set; }
        public DateTime LastReverseTime { get; set; }
        public DateTime LastRestartTime { get; set; }
        public DateTime LastZerkTime { get; set; }
        public DateTime LastSentExchangeTime { get; set; }
        public DateTime LastStallTime { get; set; }
        public DateTime LastRepairItemTime { get; set; }
        public byte CurLevel { get; set; }
        public JobType JobType { get; set; }
        public PVPMode PVPMode { get; set; }
        public byte PingCounter { get; set; } = 0;
        public bool AfkStatus { get; set; }
        public AutoInverstExp AutoInverstExp { get; set; }
        public LifeState LifeState { get; set; }
        public GameState GameState { get; set; }
        public short LatestRegion { get; set; }
        public bool AtFortressWar { get; set; } = false;
        public bool BlockStall { get; set; } = true;
        public bool CTFState { get; set; } = false;
        public bool BAState { get; set; } = false;

        public void UpdateStatusFromCharInfo(CharacterInfo characterInfo)
        {
            if (characterInfo == null) return;

            JobState = characterInfo.JobState;
            LatestRegion = characterInfo.LatestRegion;
            JobType = characterInfo.JobType;
        }
    }
}
