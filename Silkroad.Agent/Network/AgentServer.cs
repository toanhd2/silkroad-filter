﻿using Silkroad.Agent.PacketHandler.CTS;
using Silkroad.Agent.PacketHandler.CTS.Academy;
using Silkroad.Agent.PacketHandler.CTS.Auth;
using Silkroad.Agent.PacketHandler.CTS.CharAction;
using Silkroad.Agent.PacketHandler.CTS.CharSelection;
using Silkroad.Agent.PacketHandler.CTS.Enforcement;
using Silkroad.Agent.PacketHandler.CTS.Environment;
using Silkroad.Agent.PacketHandler.CTS.Events;
using Silkroad.Agent.PacketHandler.CTS.Exchange;
using Silkroad.Agent.PacketHandler.CTS.FortresWar;
using Silkroad.Agent.PacketHandler.CTS.Guild;
using Silkroad.Agent.PacketHandler.CTS.Job;
using Silkroad.Agent.PacketHandler.CTS.Logout;
using Silkroad.Agent.PacketHandler.CTS.Mastery;
using Silkroad.Agent.PacketHandler.CTS.QuestionMark;
using Silkroad.Agent.PacketHandler.CTS.Union;
using Silkroad.Agent.PacketHandler.STC.Auth;
using Silkroad.Agent.PacketHandler.STC.Character;
using Silkroad.Agent.PacketHandler.STC.Chat;
using Silkroad.Agent.PacketHandler.STC.Enforcement;
using Silkroad.Agent.PacketHandler.STC.Environment;
using Silkroad.Agent.PacketHandler.STC.Movement;
using Silkroad.Agent.PacketHandler.STC.PVP;
using Silkroad.Agent.PacketHandler.STC.QuestMark;
using Silkroad.Agent.PacketHandler.STC.Skill;
using Silkroad.Framework.Configure;
using Silkroad.Framework.Enums;
using Silkroad.Framework.Network;
using Silkroad.Framework.Security;
using System;
using System.Collections.Concurrent;

namespace Silkroad.Agent.Network
{
    public class AgentServer : Server
    {
        public PacketManager<AgentSession> PacketManager { get; set; } = new PacketManager<AgentSession>();
        public new readonly ConcurrentDictionary<Guid, AgentSession> Sessions = new ConcurrentDictionary<Guid, AgentSession>();
        public AgentServer(ServerSettings serverSettings) : base(serverSettings)
        {
        }

        protected override Session CreateSession()
        {
            return new AgentSession(this);
        }

        /// <summary>
        /// Register a new session
        /// </summary>
        /// <param name="session">Session to register</param>
        public override void RegisterSession(Session session)
        {
            // Register a new session
            Sessions.TryAdd(session.Id, session as AgentSession);
        }

        /// <summary>
        /// Unregister session by Id
        /// </summary>
        /// <param name="id">Session Id</param>
        public override void UnregisterSession(Guid id)
        {
            // Unregister session by Id
            Sessions.TryRemove(id, out _);
        }

        public override void SetupPacketHandler()
        {
            #region Client -> Server
            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_PING_REQUEST,
                new PingRequestHandler());

            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_AGENT_ENFORCEMENT_MAGIC_OPTION_GRANT_REQUEST,
                new MagicOptionGrantRequestHandler());

            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_AGENT_LOGOUT_REQUEST,
                new LogoutRequestHandler());

            PacketManager.RegisterHandler(
               PacketType.Client,
               Opcode.CLIENT_AGENT_PLAYER_BERSERK_REQUEST,
               new PlayerBerserkRequestHandler());

            PacketManager.RegisterHandler(
              PacketType.Client,
              Opcode.CLIENT_AGENT_CHARACTER_SELECTION_REQUEST,
              new CharacterSelectionRequestHandler());

            PacketManager.RegisterHandler(
              PacketType.Client,
              Opcode.CLIENT_AGENT_CHARACTER_RENAME_REQUEST,
              new CharacterRenameRequestHandler());

            PacketManager.RegisterHandler(
              PacketType.Client,
              Opcode.CLIENT_AGENT_CHARACTER_SCREEN_REQUEST,
              new CharacterScreenRequestHandler());

            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_AGENT_ENFORCEMENT_MAGIC_OPTION_GRANT_REQUEST,
                new MagicOptionGrantRequestHandler());

            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_AGENT_SIEGE_ACTION_REQUEST,
                new SiegeActionRequestHandler());

            PacketManager.RegisterHandler(
               PacketType.Client,
               Opcode.CLIENT_AGENT_UP_MASTERY_REQUEST,
               new MasteryUpdateRequestHandler());

            PacketManager.RegisterHandler(
               PacketType.Client,
               Opcode.CLIENT_AGENT_PLAYER_HANDLE_REQUEST,
               new PlayerHandleRequestHandler());

            PacketManager.RegisterHandler(
               PacketType.Client,
               Opcode.CLIENT_AGENT_GUILD_INVITE_REQUEST,
               new GuidInviteRequestHandler());

            PacketManager.RegisterHandler(
               PacketType.Client,
               Opcode.CLIENT_AGENT_UNION_INVITE_REQUEST,
               new UnionInviteRequestHandler());

            PacketManager.RegisterHandler(
               PacketType.Client,
               Opcode.CLIENT_AGENT_ACADEMY_CREATION_REQUEST,
               new AcademyCreationRequestHandler());

            PacketManager.RegisterHandler(
               PacketType.Client,
               Opcode.CLIENT_AGENT_ACADEMY_INVITE_REQUEST,
               new AcademyInviteRequestHandler());

            PacketManager.RegisterHandler(
               PacketType.Client,
               Opcode.CLIENT_AGENT_ACADEMY_JOIN_REQUEST,
               new AcademyInviteRequestHandler());

            PacketManager.RegisterHandler(
               PacketType.Client,
               Opcode.CLIENT_AGENT_ACADEMY_ACCEPT_REQUEST,
               new AcademyInviteRequestHandler());

            PacketManager.RegisterHandler(
               PacketType.Client,
               Opcode.CLIENT_AGENT_AUTH_REQUEST,
               new AuthRequestHandler());

            PacketManager.RegisterHandler(
               PacketType.Client,
               Opcode.CLIENT_AGENT_SPAWN_SUCCESS_REQUEST,
               new SpawnSuccessRequestHandler());

            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_AGENT_JOIN_JOB_REQUEST,
                new JoinJobRequestHandler());

            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_AGENT_OBJECT_ACTION_REQUEST,
                new ObjectActionRequestHandler());

            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_AGENT_EXCHANGE_START_REQUEST,
                new ExchangeRequestHandler());

            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_AGENT_EXCHANGE_ACCEPT_REQUEST,
                new ExchangeAcceptRequestHandler());

            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_AGENT_EXCHANGE_APPROVE_REQUEST,
                new ExchangeAcceptRequestHandler());

            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_AGENT_ITEM_MOVE_REQUEST,
                new ItemMoveRequestHandler());

            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_AGENT_CAPTURE_THE_FLAG_REGISTER_REQUEST,
                new CaptureTheFlagRegisterRequestHandler());

            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_AGENT_BATTLE_ARENA_REGISTER_REQUEST,
                new BattleArenaRegisterRequestHandler());

            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_AGENT_QUESTION_MARK_REQUEST,
                new QuestionMarkRequestHandler());

            PacketManager.RegisterHandler(
                PacketType.Client,
                Opcode.CLIENT_AGENT_WEATHER_REQUEST,
                new WeatherRequestHandler());
            #endregion

            #region Server -> Client
            PacketManager.RegisterHandler(
                PacketType.Server,
                Opcode.SERVER_AGENT_AUTH_RESPONSE,
                new AuthResponseHandler());

            PacketManager.RegisterHandler(
                PacketType.Server,
                Opcode.SERVER_WEATHER_RESPONSE,
                new WeatherResponseHandler());

            PacketManager.RegisterHandler(
                PacketType.Server,
                Opcode.SERVER_AGENT_ENVIRONMENT_CELESTIAL_POSITION,
                new WorldCelestialPositionResponseHandler());

            PacketManager.RegisterHandler(
                PacketType.Server,
                Opcode.SERVER_AGENT_CHARACTER_INFO_DATA_RESPONSE,
                new CharacterDataResponseHandler());

            PacketManager.RegisterHandler(
                PacketType.Server,
                Opcode.SERVER_AGENT_UNIQUE_NOTICE_RESPONSE,
                new UniqueNoticeResponseHandler());

            PacketManager.RegisterHandler(
                PacketType.Server,
                Opcode.SERVER_AGENT_CHAT_UPDATE_RESPONSE,
                new ChatUpdateResponseHandler());

            PacketManager.RegisterHandler(
               PacketType.Server,
               Opcode.SERVER_AGENT_ALCHEMY_REINFORCE_RESPONSE,
               new AlchemyReinforceResponseHandler());

            PacketManager.RegisterHandler(
               PacketType.Server,
               Opcode.SERVER_AGENT_PVP_UPDATE_RESPONSE,
               new PVPResponseHandler());

            PacketManager.RegisterHandler(
               PacketType.Server,
               Opcode.SERVER_AGENT_QUESTION_MARK_RESPONSE,
               new QuestionMarkResponseHandler());

            PacketManager.RegisterHandler(
               PacketType.Server,
               Opcode.SERVER_AGENT_MOVEMENT_RESPONSE,
               new MovementResponseHandler());

            PacketManager.RegisterHandler(
               PacketType.Server,
               Opcode.SERVER_AGENT_CHANGE_STATUS_RESPONSE,
               new ChangeStatusResponseHandler());

            PacketManager.RegisterHandler(
               PacketType.Server,
               Opcode.SERVER_AGENT_CAST_SKILL_RESPONSE,
               new CastSkillResponseHandler());
            #endregion
        }
    }
}
